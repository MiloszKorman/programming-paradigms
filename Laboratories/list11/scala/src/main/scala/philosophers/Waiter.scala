package philosophers

import akka.actor.{Actor, ActorRef}

class Waiter extends Actor {
  case class Customer(name: String, var isEating: Boolean, ref: ActorRef)
  var customers: List[Customer] = Nil

  override def receive: Receive = {
    case Introduce(name) =>
      println(name + " introduced himself")
      customers = Customer(name, isEating = false, sender)::customers
    case Hungry =>
      val customer = customers.find(cust => cust.ref == sender).get
      val customerIndex = customers.indexOf(customer)
      val leftNeighbour = if (customerIndex == 0) customers.last else customers(customerIndex-1)
      val rightNeighbour = if (customerIndex == customers.length-1) customers.head else customers(customerIndex+1)
      if (!leftNeighbour.isEating && !rightNeighbour.isEating) {
        customer.isEating = true
        customer.ref ! Granted
      } else {
        customer.ref ! Denied
      }
    case Finished =>
      customers.find(cust => cust.ref == sender).get.isEating = false
  }
}
