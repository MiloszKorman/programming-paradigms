package philosophers

import akka.actor.{ActorSystem, Props}

object PhilosophersProblem {
  def main(args: Array[String]): Unit = {
    val problem = ActorSystem("PhilosophersProblem")
    val waiter = problem.actorOf(Props[Waiter])

    problem.actorOf(Props(classOf[Philosopher], "Philosopher 1", waiter))
    problem.actorOf(Props(classOf[Philosopher], "Philosopher 2", waiter))
    problem.actorOf(Props(classOf[Philosopher], "Philosopher 3", waiter))
    problem.actorOf(Props(classOf[Philosopher], "Philosopher 4", waiter))
    problem.actorOf(Props(classOf[Philosopher], "Philosopher 5", waiter))
  }
}
