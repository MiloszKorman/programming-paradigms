package philosophers

case object Granted
case object Denied
case object Hungry
case object Finished
case class Introduce(name: String)
