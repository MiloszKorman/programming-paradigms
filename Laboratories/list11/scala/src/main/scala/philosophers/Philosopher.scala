package philosophers

import akka.actor.{Actor, ActorRef}

class Philosopher(name: String, waiter: ActorRef) extends Actor{

  waiter ! Introduce(name)
  think()

  override def receive: Receive = {
    case Granted =>
      eat()
    case Denied =>
      think()
  }

  private def think(): Unit = {
    println(name + " thinking")
    Thread.sleep(400)
    waiter ! Hungry
  }

  private def eat(): Unit = {
    println(name + " eating")
    Thread.sleep(200)
    waiter ! Finished
    think()
  }
}
