package prod_cons

import akka.actor.{ActorSystem, Props}

object ProdConsProblem {
  def main(args: Array[String]): Unit = {
    val problem = ActorSystem("MyActorSystem")
    val buff = problem.actorOf(Props(classOf[Buffer]))
    val prod = problem.actorOf(Props(classOf[Producer], buff))
    val cons = problem.actorOf(Props(classOf[Consumer], buff))

    prod ! Declined
    cons ! BuffEmpty
  }
}
