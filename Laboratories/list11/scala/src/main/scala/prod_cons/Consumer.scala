package prod_cons

import akka.actor.{Actor, ActorRef}

class Consumer(buff: ActorRef) extends Actor {
  override def receive: Receive = {
    case BuffEmpty =>
      buff ! Inquiry
    case Message(msg) =>
      println("Consumer received message: " + msg)
      buff ! Inquiry
  }
}
