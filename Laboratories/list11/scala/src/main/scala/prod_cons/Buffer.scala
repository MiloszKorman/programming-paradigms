package prod_cons

import akka.actor.Actor

class Buffer extends Actor {
  private var msg: String = ""

  override def receive: Receive = {
    case Inquiry =>
      if (msg.isEmpty) {
        sender ! BuffEmpty
      } else {
        val toSend = msg
        msg = ""
        sender ! Message(toSend)
      }
    case Message(message) =>
      if (msg.isEmpty) {
        msg = message
        sender ! Accepted
      } else {
        sender ! Declined
      }
  }
}
