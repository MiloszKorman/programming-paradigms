package prod_cons

// Communication Consumer-Buffer
case object BuffEmpty
case object Inquiry

// Communication Producer-Buffer
case object Accepted
case object Declined
case class Message(msg: String)
