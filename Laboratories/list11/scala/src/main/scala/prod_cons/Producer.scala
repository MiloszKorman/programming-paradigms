package prod_cons

import akka.actor.{Actor, ActorRef}

class Producer(buff: ActorRef) extends Actor {
  private var counter: Int = 1

  override def receive: Receive = {
    case Declined =>
      buff ! Message(counter.toString)
    case Accepted =>
      println("Producers msg: " + counter + " has been accepted")
      counter += 1
      buff ! Message(counter.toString)
  }
}
