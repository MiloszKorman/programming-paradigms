class Person(name: String) {
  def getName: String = name
}
case class Student(name: String) extends Person(name)
case class Lecturer(name: String) extends Person(name)

object CovarianceTest {
  val students: List[Student] = List(Student("Milosz"), Student("Bartosz"))
  val lecturers: List[Lecturer] = List(Lecturer("Jurek"), Lecturer("Damian"))

  def printPeople(people: List[Person]): Unit = {
    people.foreach(println)
  }

  //List is Covariant: sealed abstract class List[+A]

  def test(): Unit = {
    println("Print list of students in function accepting list of people")
    CovarianceTest.printPeople(students)
    println("Print list of lecturers in function accepting list of people")
    CovarianceTest.printPeople(lecturers)
  }
}

abstract class Printer[-T] {
  def print(value: T): Unit
}

class PersonPrinter extends Printer[Person] {
  override def print(person: Person): Unit = {
    println("Persons name is: " + person.getName)
  }
}

class StudentPrinter extends Printer[Student] {
  override def print(student: Student): Unit = {
    println("Students name is: " + student.name)
  }
}

object ContravarianceTest {
  val student: Student = Student("boniek")

  def printStudent(printer: Printer[Student], student: Student): Unit = {
    printer.print(student)
  }

  def test(): Unit = {
    println("Print a student using a person printer")
    ContravarianceTest.printStudent(new PersonPrinter, student)
    println("Print a student using a student printer")
    ContravarianceTest.printStudent(new StudentPrinter, student)
  }
}

class Container[A](value: A) {
  private var _value: A = value
  def getValue: A = _value
  def setValue(value: A): Unit = {
    _value = value
  }
}

object InvarianceTest {
  def test(): Unit = {
    val studentContainer: Container[Student] = new Container[Student](Student("boniek"))
    //val personContainer: Container[Person] = studentContainer //boom
  }
}

object FunctionTest {

  val personToPerson = (p: Person) => p
  val personToStudent = (p: Person) => new Student(p.getName)
  val studentToPerson = (s: Student) => new Person(s.name)
  val studentToStudent = (s: Student) => s

  def test(): Unit = {
    var pTP: Person => Person = personToStudent //ok because student is a subtype of person
    //pTP = studentToPerson //not ok, argument needs to be person or super class
    //pTP = studentToStudent //not ok, argument needs to be person or super class
    pTP = personToPerson //ok because as declared

    var sTS: Student => Student = personToStudent //ok because argument needs to be student or super class
    //sTS = personToPerson //not ok, return must be student or sub class
    //sTS = studentToPerson //not ok, return must be student or sub class
    sTS = studentToStudent //ok because as declared

    var pTS: Person => Student = personToStudent //ok because as declared
    //pTS = personToPerson //not ok, return must be student or sub class
    //pTS = studentToPerson //not ok, because argument needs to be person or super class
    //pTS = studentToStudent //not ok, because argument needs to be person or super class

    var sTP: Student => Person = personToPerson //ok because argument needs to be student or super class4
    sTP = personToStudent //ok because argument needs to be student or super class, return person or sub class
    sTP = studentToPerson //ok because as declared
    sTP = studentToStudent //ok because return must be person or sub class
  }
}

object main {
  def main(args: Array[String]): Unit = {
    println("Covariance test")
    CovarianceTest.test()
    println("\n")
    println("Contravariance test")
    ContravarianceTest.test()
    /*println("\n")
    println("Invariance test")
    InvarianceTest.test()*/
  }
}
