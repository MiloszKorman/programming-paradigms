type 'a bt = Empty | Node of 'a * 'a bt * 'a bt;;

let tt = Node(8, 
              Node(3, 
                   Node(1, 
                        Empty, 
                        Empty
                       ), 
                   Node(6, 
                        Node(4, 
                             Empty, 
                             Empty
                            ), 
                        Node(7, 
                             Empty, 
                             Empty
                            )
                       )
                  ), 
              Node(10, 
                   Empty, 
                   Node(14, 
                        Node(13, 
                             Empty, 
                             Empty
                            ), 
                        Empty
                       )
                  )
             );;

let rec sum_tree = function
    Empty -> 0
  | Node(v, lt, rt) -> v + sum_tree lt + sum_tree rt;;

sum_tree tt;; (* 66 *)
sum_tree Empty;; (* 0 *)
sum_tree (Node(1, Empty, Empty));; (* 1 *)
