type 'a bt = Empty | Node of 'a * 'a bt * 'a bt;;

let tt = Node(8, 
              Node(3, 
                   Node(1, 
                        Empty, 
                        Empty
                       ), 
                   Node(6, 
                        Node(4, 
                             Empty, 
                             Empty
                            ), 
                        Node(7, 
                             Empty, 
                             Empty
                            )
                       )
                  ), 
              Node(10, 
                   Empty, 
                   Node(14, 
                        Node(13, 
                             Empty, 
                             Empty
                            ), 
                        Empty
                       )
                  )
             );;

let rec prod_tree = function
    Empty -> 1
  | Node(v, lt, rt) -> v * prod_tree lt * prod_tree rt;;

prod_tree tt;; (* 7338240 *)
prod_tree Empty;; (* 1 *)
prod_tree (Node(1, Empty, Empty));; (* 1 *)
prod_tree (Node((-1), Empty, Empty));; (* -1 *)
