type 'a bt = Empty | Node of 'a * 'a bt * 'a bt;;

let tt = Node(8, 
              Node(3, 
                   Node(1, 
                        Empty, 
                        Empty
                       ), 
                   Node(6, 
                        Node(4, 
                             Empty, 
                             Empty
                            ), 
                        Node(7, 
                             Empty, 
                             Empty
                            )
                       )
                  ), 
              Node(10, 
                   Empty, 
                   Node(14, 
                        Node(13, 
                             Empty, 
                             Empty
                            ), 
                        Empty
                       )
                  )
             );;

let rec contains t searched = match t with
    Empty -> false
  | Node(v, lt, rt) -> 
      if v = searched then true
      else contains lt searched || contains rt searched;;

contains tt 8;;
contains tt 3;;
contains tt 1;;
contains tt 6;;
contains tt 10;;
contains tt 13;;
not (contains tt 15);;
not (contains tt (-1));;
