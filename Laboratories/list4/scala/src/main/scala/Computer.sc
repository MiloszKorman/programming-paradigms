case class Computer(containsComponent: Int => Boolean)

val myComputer = Computer((i: Int) => i match {
  case 0 => true
  case 1 => false
  case 2 => true
  case 3 => true
  case 4 => false
  case 5 => true
  case 6 => false
  case _ => throw new Exception("Such component does not exist")
})

myComputer.containsComponent(1) //false
myComputer.containsComponent(2) //true
myComputer.containsComponent(7) //exception