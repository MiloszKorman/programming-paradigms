import scala.Numeric.Implicits._

sealed trait Expression[T]
case class Value[T](value: T) extends Expression[T]
case class Add[T](first: Expression[T], second: Expression[T]) extends Expression[T]
case class Negate[T](first: Expression[T]) extends Expression[T]

def calculator[T: Numeric](expression: Expression[T]): T = expression match {
  case Value(value) => value
  case Add(first, second) => calculator(first) + calculator(second)
  case Negate(first) => -calculator(first)
}

calculator(Add(Value(1), Value(2)))
calculator(Negate(Add(Add(Value(1), Value(5)), Value(4))))
calculator(Value(1))
calculator(Negate(Value(-1)))
calculator(Add(Value(1.2), Add(Value(3.2), Value(1.4))))
