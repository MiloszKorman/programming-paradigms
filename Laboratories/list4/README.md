Algebraiczne typy danych

_Dla każdego zadania przygotuj odpowiednie testy pokazujące prawidłowość
działania zaimplementowanej funkcji_.

**Zad 1. – 3 pkt. (Ocaml)**

Dla drzewa BST (Binary Search Tree):

```
a) Napisz funkcję contains typu int_tree -> int -> bool sprawdzającą, czy
drzewo zawiera daną liczbę.
b) Napisz funkcję sum_tree typu int_tree -> int zwracającą sumę elementów
w drzewie.
c) Napisz funkcję prod_tree typu int_tree -> int zwracającą iloczyn
elementów w drzewie.
```
**Zad 2. – 4 pkt. (Scala)**

_Za pomocą ADT i pattern maching’u_ napisz prosty kalkulator, _który obsługuje dwie_
funkcje: dodawania (a+b) i negacji (-n).
