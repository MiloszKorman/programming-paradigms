type 'a llist = LKoniec | LElement of 'a * (unit -> 'a llist);;

let rec ltake = function
    (0, _) -> []
  | (_, LKoniec) -> []
  | (n, LElement(x, xf)) -> x::ltake(n-1, xf());;

let rec lfrom k = LElement (k, fun () -> lfrom (k+1));;

let lpodziel ll = 
  let rec getOdd = function
      LKoniec -> LKoniec
    | LElement(v, nl) -> 
        if v mod 2 <> 0 then LElement(v, fun () -> getOdd (nl())) 
        else getOdd (nl())
  in
  let rec getEven = function
      LKoniec -> LKoniec
    | LElement(v, nl) -> 
        if v mod 2 = 0 then LElement(v, fun () -> getEven (nl())) 
        else getEven (nl())
  in (getOdd ll, getEven ll);;
(*if (v mod 2 = 0) then (fst (lpodziel (nl())), LElement(v, fun () -> snd (lpodziel (nl()))))
  else (LElement(v, fun () -> fst (lpodziel (nl()))), snd (lpodziel (nl())));;*)

lpodziel (lfrom 20);;
ltake (5, fst (lpodziel (lfrom 10)));; 
ltake (5, snd (lpodziel (lfrom 10)));; 
