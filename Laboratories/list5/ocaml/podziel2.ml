type 'a nlist = Koniec | Element of 'a * ('a nlist);;

let podziel l =
  let rec inPodziel = function
      (Koniec, _) -> (Koniec, Koniec)
    | (Element(v, nl), false) -> let next = inPodziel (nl, true) in (Element(v, fst next), snd next)
    | (Element(v, nl), true) -> let next = inPodziel (nl, false) in (fst next, Element(v, snd next))
  in inPodziel (l, false);;

let (evenI1, oddI1) = podziel (Element(5, Element(6, Element(3, Element(2, Element(1, Koniec))))));; (* ((5,3,1), (6,2)) *)
let (evenI2, oddI2) = podziel (Element(1, Element(2, Element(1, Element(2, Koniec)))));; (* ((1, 1), (2, 2)) *)
let (evenI3, oddI3) = podziel Koniec;; (* ((), ()) *)

type 'a llist = LKoniec | LElement of 'a * (unit -> 'a llist);;

let rec ltake = function                                                                                                              
    (0, _) -> []                                                                                                                      
  | (_, LKoniec) -> []                                                                                                                
  | (n, LElement(x, xf)) -> x::ltake(n-1, xf());;                                                                                     

let rec lfrom k = LElement (k, fun () -> lfrom (k+1));;  

let lpodziel ll =
  let rec inLPodziel = function
      (LKoniec, _) -> LKoniec
    | (LElement(v, nll), false) -> inLPodziel (nll(), true)
    | (LElement(v, nll), true) -> LElement(v, fun () -> inLPodziel (nll(), false))
  in (inLPodziel (ll, true), inLPodziel (ll, false));;

let (lEven1, lOdd1) = lpodziel (lfrom 20);;
ltake (5, lEven1);; (* [20; 22; 24; 26; 28] *)
ltake (5, lOdd1);; (* [21; 23; 25; 27; 29] *)
let (lEven2, lOdd2) = lpodziel (LElement(1, fun () -> LElement(2, fun () -> LElement(1, fun () -> LElement(2, fun () -> LKoniec)))));;
ltake (100, lEven2);; (* [1;1] *)
ltake (100, lOdd2);; (* [2;2] *)
let tll3 = lpodziel(LKoniec);; (* (LKoniec, LKoniec) *)
