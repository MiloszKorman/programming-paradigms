type 'a nlist = Koniec | Element of 'a * ('a nlist);;

let rec podziel = function
    Koniec -> (Koniec, Koniec)
  | Element(v, nl) -> 
      let nP = podziel nl in
        if v mod 2 = 0 then (fst nP, Element(v, snd nP))
        else (Element(v, fst nP), snd nP);;

podziel (Element(5, Element(6, Element(3, Element(2, Element(1, Koniec))))));; (* ((5,3,1), (6,2)) *)
podziel (Element(5, Element(3, Element(1, Koniec))));; (* ((5,3,1), ()) *)
podziel (Element(6, Element(2, Koniec)));; (* ((), (6,2)) *)
