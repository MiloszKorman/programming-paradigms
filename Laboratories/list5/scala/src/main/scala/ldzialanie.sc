def ldzialanie [A] (s1: Stream[A])(s2: Stream[A])(op: A => A => A): Stream[A] =
  (s1, s2) match {
  case (ll1, Stream.Empty) => ll1
  case (Stream.Empty, ll2) => ll2
  case (v1#::ll1, v2#::ll2) => op(v1)(v2)#::ldzialanie(ll1)(ll2)(op)
}

ldzialanie(Stream.from(3))(Stream.from(7))((a: Int) => (b: Int) => a + b).take(15).force

ldzialanie(List(1,2,3).toStream)(List(2,3,4,5).toStream)((a: Int) => (b: Int) => a + b).force

ldzialanie(Stream.from(2))(Stream.empty)((a: Int) => (b: Int) => a + b).take(10).force