let isDescending (fst, snd, trd) = (trd < snd) && (snd < fst);;

print_string (string_of_bool (isDescending (3, 2, 1)));; (* true *)
print_string "\n";;
print_string (string_of_bool (isDescending (1, 2, 3)));; (* false *)
print_string "\n";;
print_string (string_of_bool (isDescending (0, -1, -2)));; (* true *)
print_string "\n";;
print_string (string_of_bool (isDescending (0, 0, 0)));; (* false *)