let rec listSumSimple (realNumbers: float list): float = 
  List.fold_left (fun acc elem -> acc +. elem) 0.0 realNumbers;;

print_float (listSumSimple(1.0::4.0::3.2::[]));; (* expected 8.2 *)
print_string "\n";;
print_float (listSumSimple([]));; (* expected 0 *)
print_string "\n";;

let rec listSum(realNumbers: float list): float =
  if realNumbers = [] then 0.0
  else (List.hd realNumbers) +. listSum((List.tl realNumbers));;

print_float (listSum(1.0::4.0::3.2::[]));; (* expected 8.2 *)
print_string "\n";;
print_float (listSum([]));; (* expected 0 *)
print_string "\n";;