let rec filterGreaterSimple (list, border) = List.filter (fun x -> border < x) list;;

print_string (String.concat " " (List.map string_of_int (filterGreaterSimple (1::2::3::[], 1))));;
print_string "\n";;

let rec filterGreater (lista, border) = 
  if (lista = []) then []
  else if ((List.hd lista) > border) then (List.hd lista)::filterGreater(List.tl lista, border)
  else filterGreater(List.tl lista, border);;

print_string (String.concat " " (List.map string_of_int (filterGreater (1::2::3::[], 1))));;