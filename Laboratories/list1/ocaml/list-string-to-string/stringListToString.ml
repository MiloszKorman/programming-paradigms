let rec mergerSimple (separator, list) = 
  if (list = []) then ""
  else  List.fold_left (fun acc elem -> if (acc = "") then elem else acc^separator^elem) "" list;;

print_string(mergerSimple(",", "juras"::"oguras"::[]));;
print_string "\n";;
print_string(mergerSimple(";", "mirek"::"kirek"::"żwirek"::[]));;
print_string "\n";;

let rec merger (separator, list) = 
  if (list = []) then ""
  else if ((List.tl list) = []) then (List.hd list)
  else (List.hd list) ^ separator ^ merger(separator, (List.tl list));;

print_string(merger(",", "juras"::"oguras"::[]));;
print_string "\n";;
print_string(merger(";", "mirek"::"kirek"::"żwirek"::[]));;