### Zad 1. (OCaml i Scala)

Napisz funkcję, która przyjmuje na wejściu listę liczb rzeczywistych, a następnie zwraca sumę tych
liczb. Proszę przygotować minimalny zestaw testów dla tej funkcji prezentujący jej działanie. (Pusta
lista to też lista).

### Zad 2. (OCaml)

Napisz funkcję, która przejmuje dwa parametry: napis (będący separatorem) oraz lista napisów.
Funkcja ta ma zwracać pojedynczy napis składający się z napisów tablicy oddzielonych separatorem.

### Zad 3. (Scala)

Napisz funkcję, która przyjmuje dwa parametry: listę liczb całkowitych i liczbę całkowitą (n). Ma ona
zwracać tablicę tylko z liczbami większymi niż zadana liczba n, które znajdowały się w tablicy będącej
pierwszym parametrem.

### Zad 4. (OCaml lub Scala)

Napisz funkcję przyjmującą 3 parametry. Funkcja ma zwracać wartość boolowska (true/false) w
zależności od tego czy liczby są ustawione malejąco czy też nie.

### Zad 5. (OCaml lub Scala – inny niż zad 4)

Napisz funkcję przyjmującą znak i listę znaków i liczącą liczbę wystąpień tego znaku
