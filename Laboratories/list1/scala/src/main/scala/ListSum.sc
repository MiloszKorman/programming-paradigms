def listSumSimple(realNumbers: List[Double]):Double = realNumbers.sum

listSumSimple(List())
listSumSimple(List(2.3, 4.51, -1.13))

def listSum(realNumbers: List[Double]): Double = realNumbers match {
  case Nil => 0.0
  case head::tail => head + listSum(tail)
}

listSum(List()) // expected - 0.0
listSum(List(1.3, 2.2, 3.3)) // expected - 6.8
listSum(List(2.3, 4.51, -1.13)) //expected - 5.68