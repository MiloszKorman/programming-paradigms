def mergerSimple(separator: String, list: List[String]): String =
  if (list == Nil) "" else list.reduce(_ + separator + _)


mergerSimple(", ", List()) //expected - ""
mergerSimple(":", List("1", "2", "3")) //expected - "1:2:3"
mergerSimple(" ", List("another", "text")) //expected - "another text"

def merger(separator: String, list: List[String]): String = list match {
  case Nil => ""
  case head::Nil => head
  case head::tail => head + separator + merger(separator, tail)
}

merger(", ", List()) //expected - ""
merger(":", List("1", "2", "3")) //expected - "1:2:3"
merger(" ", List("another", "text")) //expected - "another text"