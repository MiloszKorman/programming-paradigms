def filterGreaterSimple(list: List[Int], border: Int): List[Int] = list.filter(_ > border)

filterGreaterSimple(Nil, 13) //expected - List()
filterGreaterSimple(List(-15, -3, 0, 1, 14), -4) //expected - List(-3, 0, 1, 14)

def filterGreater(list: List[Int], border:Int): List[Int] = {
  if (list == Nil) Nil
  else if (list.head > border) list.head::filterGreater(list.tail, border)
  else filterGreater(list.tail, border)
}

filterGreater(Nil, 13) //expected - List()
filterGreater(List(-15, -3, 0, 1, 14), -4) //expected - List(-3, 0, 1, 14)
