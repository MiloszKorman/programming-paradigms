//Int, Double or what??
def isDescending(fst: Int, snd: Int, trd: Int):Boolean = (trd < snd) && (snd < fst)

isDescending(1, 2, 3) //expected - false
isDescending(0, -1, -2) //expected - true
isDescending(2, 13, 1) // expected - false
isDescending(13, 2, 21) //expected - false
isDescending(4, 2, 0) //expected - true