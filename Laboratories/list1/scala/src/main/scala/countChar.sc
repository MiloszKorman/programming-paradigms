def countCharSimple(searchedFor: Char, list: List[Char]): Int = list.count(_==searchedFor)

countCharSimple('*', 'a'::'b'::'*'::'*'::'9'::Nil); //expected - 2
countCharSimple('*', Nil) //expected - 0
countCharSimple('*', List('*')) //expected - 1
countCharSimple('\n', List('a', '1', '2', '\n', '\t', '\n')) //expected - 2

def countChar(searchedFor: Char, list: List[Char]): Int = list match {
  case Nil => 0
  case head::tail => if (head == searchedFor) 1 + countChar(searchedFor, tail) else countChar(searchedFor, tail)
}

countChar('*', 'a'::'b'::'*'::'*'::'9'::Nil); //expected - 2
countChar('*', Nil) //expected - 0
countChar('*', List('*')) //expected - 1