import coffee.machine.CoffeeMachineMenu;

public class Main {
    public static void main(String[] args) {
        (new CoffeeMachineMenu()).run();
    }
}
