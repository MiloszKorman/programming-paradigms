package shared.products;

public class Water extends MeasuredObject implements Drinkable {

    public Water(int quantity) {
        super(quantity);
    }

    @Override
    public void drink() {
        System.out.println(quantity + "ml of water drank");
    }

    @Override
    public Water split(int howMuch) {
        int toGive = (this.quantity > howMuch) ? howMuch : this.quantity;
        this.quantity -= toGive;
        return new Water(toGive);
    }
}
