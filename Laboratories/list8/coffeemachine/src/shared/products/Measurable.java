package shared.products;

public interface Measurable {
    int getQuantity();
}
