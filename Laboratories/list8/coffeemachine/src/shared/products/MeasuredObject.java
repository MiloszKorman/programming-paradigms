package shared.products;

public abstract class MeasuredObject implements Measurable {
    protected int quantity;

    public MeasuredObject(int quantity) {
        if (quantity < 0) {
            quantity = 0;
        }
        this.quantity = quantity;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    public void add(MeasuredObject toAdd,  int howMuch) {
        if (this.getClass().equals(toAdd.getClass())) {
            int toTake = (howMuch > toAdd.quantity) ? toAdd.quantity : howMuch;
            this.quantity += toTake;
            toAdd.quantity -= toTake;
        }
    }

    public  abstract MeasuredObject split(int howMuch);
}
