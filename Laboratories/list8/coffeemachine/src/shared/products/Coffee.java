package shared.products;

public class Coffee extends MeasuredObject implements Drinkable {
    private final CoffeeKind kind;
    private Milk myMilk;

    public Coffee(int quantity, CoffeeKind kind) {
        super(quantity);
        this.kind = kind;
        this.myMilk = null;
    }

    @Override
    public void drink() {
        System.out.println(quantity + "ml of " + kind.toString() + " coffee drank");
        this.quantity = 0;
    }

    public void addMilk(Milk milk, int amount) {
        Milk myMilk = milk.split(amount);
        this.quantity += myMilk.quantity;
    }

    @Override
    public Coffee split(int howMuch) {
        int toGive = (this.quantity > howMuch) ? howMuch : this.quantity;
        if (this.myMilk == null) {
            return new Coffee(toGive, this.kind);
        } else {
            double milkRatio = myMilk.getQuantity() / (double)this.quantity;
            int milkToGive = (int)milkRatio * toGive;
            Coffee toReturn = new Coffee(toGive - milkToGive, this.kind);
            toReturn.addMilk(this.myMilk.split(milkToGive), milkToGive);
            this.quantity -= toGive;
            return toReturn;
        }
    }

    @Override
    public int getQuantity() {
        return super.getQuantity();
    }
}
