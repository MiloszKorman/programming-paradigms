package shared.products;

public class Milk extends MeasuredObject implements Drinkable {
    private boolean foamed;

    public Milk(int quantity) {
        super(quantity);
        this.foamed = false;
    }

    @Override
    public void drink() {
        System.out.println(quantity + "ml of milk drank");
    }

    public boolean isFoamed() {
        return foamed;
    }

    public void foam() {
        this.foamed = true;
    }

    @Override
    public Milk split(int howMuch) {
        int toGive = (this.quantity > howMuch) ? howMuch : this.quantity;
        this.quantity -= toGive;
        Milk toReturn = new Milk(toGive);
        toReturn.foamed = this.foamed;
        return toReturn;
    }
}
