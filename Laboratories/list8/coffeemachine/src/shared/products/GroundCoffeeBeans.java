package shared.products;

public class GroundCoffeeBeans extends MeasuredObject {
    public GroundCoffeeBeans(int quantity) {
        super(quantity);
    }

    @Override
    public GroundCoffeeBeans split(int howMuch) {
        int toGive = (this.quantity > howMuch) ? howMuch : this.quantity;
        this.quantity -= toGive;
        return new GroundCoffeeBeans(toGive);
    }
}
