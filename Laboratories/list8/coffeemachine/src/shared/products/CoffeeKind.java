package shared.products;

public enum CoffeeKind {
    Americano(500, 0, 14),
    Latte(200, 300, 7),
    Cappuccino(300, 200, 7),
    Espresso(30, 0, 7),
    Macchiato(200, 100, 14);

    private final int waterAmount;
    private final int milkAmount;
    private final int groundBeansAmount;
    private boolean milkFoamed;

    CoffeeKind(int waterAmount, int milkAmount, int groundBeansAmount) {
        this.waterAmount = waterAmount;
        this.milkAmount = milkAmount;
        this.groundBeansAmount = groundBeansAmount;
        this.milkFoamed = false;
    }

    public void foam() {
        this.milkFoamed = true;
    }

    public int getWaterAmount() {
        return waterAmount;
    }

    public int getMilkAmount() {
        return milkAmount;
    }

    public int getGroundBeansAmount() {
        return groundBeansAmount;
    }

    public boolean isMilkFoamed() {
        return milkFoamed;
    }
}
