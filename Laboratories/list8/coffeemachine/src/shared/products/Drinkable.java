package shared.products;

public interface Drinkable {
    void drink();
}
