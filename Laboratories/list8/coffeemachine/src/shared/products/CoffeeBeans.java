package shared.products;

public class CoffeeBeans extends MeasuredObject {
    public CoffeeBeans(int quantity) {
        super(quantity);
    }

    @Override
    public CoffeeBeans split(int howMuch) {
        int toGive = (this.quantity > howMuch) ? howMuch : this.quantity;
        this.quantity -= toGive;
        return new CoffeeBeans(toGive);
    }
}
