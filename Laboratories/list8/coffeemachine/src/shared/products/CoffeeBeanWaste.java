package shared.products;

public class CoffeeBeanWaste extends MeasuredObject {
    public CoffeeBeanWaste(int quantity) {
        super(quantity);
    }

    @Override
    public CoffeeBeanWaste split(int howMuch) {
        int toGive = (this.quantity > howMuch) ? howMuch : this.quantity;
        this.quantity -= toGive;
        return new CoffeeBeanWaste(toGive);
    }
}
