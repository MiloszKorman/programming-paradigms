package shared;

import java.util.Scanner;

public class Tools {
    private Tools() {
    }

    public static int nextInt(Scanner sc) {
        boolean proper = false;
        int result = 0;

        while (!proper) {
            try {
                result = Integer.parseInt(sc.nextLine());
                proper = true;
            } catch (NumberFormatException e) {
                System.out.println("Not a proper number");
                proper = false;
            }
        }

        return result;
    }

    public static int nextIntBetween(int bottom, int top, Scanner sc) {
        int result = 0;
        boolean correct = false;

        while (!correct) {
            result = nextInt(sc);
            if (bottom <= result && result <= top) {
                correct = true;
            } else {
                System.out.println("Out of bounds");
            }
        }

        return result;
    }

    public static boolean nextBoolean(Scanner sc) {
        return sc.nextLine().toUpperCase().charAt(0) == 'Y';
    }
}

