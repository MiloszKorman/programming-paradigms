package shared.items;

import coffee.machine.impl.UndrinkableException;
import shared.products.Drinkable;
import shared.products.MeasuredObject;

public class Container<E extends MeasuredObject> implements Emptiable, Fullable {

    private final int capacity;
    private E stored;

    public Container(E toStore) {
        this.capacity = toStore.getQuantity();
        this.stored = toStore;
    }

    public Container(int capacity, E toStore) {
        this.capacity = (capacity > toStore.getQuantity()) ? capacity : toStore.getQuantity();
        this.stored = toStore;
        if (this.stored.getQuantity() > this.capacity) {
            this.stored.split(this.stored.getQuantity() - this.capacity);
        }
    }

    public void drink() throws UndrinkableException {
        if (stored instanceof Drinkable) {
            ((Drinkable) stored).drink();
        } else {
            throw new UndrinkableException();
        }
    }

    public E getStored() {
        return stored;
    }

    public int getQuantity() {
        return stored.getQuantity();
    }

    public void addMore(E toAdd) {
        stored.add(toAdd, capacity - this.stored.getQuantity());
    }

    @SuppressWarnings("unchecked")
    public E getSome(int howMuch) {
        return (E)stored.split(howMuch);
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public boolean isEmpty() {
        return stored.getQuantity() == 0;
    }

    @Override
    public boolean isFull() {
        return stored.getQuantity() == this.capacity;
    }
}
