package coffee.machine.impl;

import shared.products.Milk;

class Frother {
    void froth(Milk milk) {
        milk.foam();
    }
}
