package coffee.machine.impl;

import shared.products.CoffeeBeans;
import shared.products.GroundCoffeeBeans;

class Grinder {
    GroundCoffeeBeans grind(CoffeeBeans beans) {
        return new GroundCoffeeBeans(beans.getQuantity());
    }
}
