package coffee.machine.impl;

import shared.items.Container;
import shared.products.*;

class Dripper {

    private final Container<GroundCoffeeBeans> groundsContainer;
    private final Container<CoffeeBeanWaste> wasteContainer;

    Dripper(Container<GroundCoffeeBeans> groundsContainer, Container<CoffeeBeanWaste> wasteContainer) {
        this.groundsContainer = groundsContainer;
        this.wasteContainer = wasteContainer;
    }

    Coffee drip(Water water, CoffeeKind kind) {
        groundsContainer.getSome(kind.getGroundBeansAmount());
        wasteContainer.addMore(new CoffeeBeanWaste(kind.getGroundBeansAmount()));
        return new Coffee(water.getQuantity(), kind);
    }

    boolean wastePresent() {
        return wasteContainer.getQuantity() != 0;
    }

    CoffeeBeanWaste emptyWaste() {
        return wasteContainer.getSome(wasteContainer.getQuantity());
    }

    int groundsQuantity() {
        return groundsContainer.getQuantity();
    }

    void addGrounds(GroundCoffeeBeans groundBeans) {
        groundsContainer.addMore(groundBeans);
    }

    int getGroundsContainerCapacity() {
        return groundsContainer.getCapacity();
    }
}
