package coffee.machine.impl;

import coffee.machine.impl.exceptions.NotEnoughBeansException;
import coffee.machine.impl.exceptions.NotEnoughMilkException;
import coffee.machine.impl.exceptions.NotEnoughWaterException;
import coffee.machine.impl.exceptions.WasteException;
import shared.items.Container;
import shared.products.*;

public class CoffeeMachine {

    private final Container<Water> waterContainer;
    private final Container<Milk> milkContainer;
    private final Container<CoffeeBeans> beansContainer;
    private final Dripper dripper;
    private final Grinder grinder;
    private final Frother frother;

    public CoffeeMachine() {
        this.waterContainer = new Container<>(new Water(1000));
        this.milkContainer = new Container<>(new Milk(1000));
        this.beansContainer = new Container<>(new CoffeeBeans(100));
        this.dripper = new Dripper(
                new Container<>(new GroundCoffeeBeans(20)),
                new Container<>(20, new CoffeeBeanWaste(0)));
        this.grinder = new Grinder();
        this.frother = new Frother();
    }

    public Coffee makeCoffee(CoffeeKind kind)
            throws NotEnoughBeansException, NotEnoughMilkException, NotEnoughWaterException, WasteException {
        if (!dripper.wastePresent()) {
            if (waterContainer.getQuantity() >= kind.getWaterAmount()) {
                if (milkContainer.getQuantity() >= kind.getMilkAmount()) {
                    if (beansContainer.getQuantity() + dripper.groundsQuantity() >= kind.getGroundBeansAmount()) {
                        if (dripper.groundsQuantity() < kind.getGroundBeansAmount()) {
                            dripper.addGrounds(
                                    grinder.grind(
                                            beansContainer.getSome(
                                                    dripper.getGroundsContainerCapacity() - dripper.groundsQuantity()
                                            )
                                    )
                            );
                        }
                        Coffee made = dripper.drip(waterContainer.getSome(kind.getWaterAmount()), kind);
                        Milk milkToAdd = milkContainer.getSome(kind.getMilkAmount());
                        if (kind.isMilkFoamed()) {
                            frother.froth(milkToAdd);
                        }
                        made.addMilk(milkToAdd, kind.getMilkAmount());
                        System.out.println("### After: " + beansContainer.getQuantity()); // TODO delete
                        return made;
                    } else {
                        throw new NotEnoughBeansException();
                    }
                } else {
                    throw new NotEnoughMilkException();
                }
            } else {
                throw new NotEnoughWaterException();
            }
        } else {
            throw new WasteException();
        }
    }

    public void refillWater(Water water) {
        this.waterContainer.addMore(water);
    }

    public void refillMilk(Milk milk) {
        this.milkContainer.addMore(milk);
    }

    public void refillBeans(CoffeeBeans beans) {
        this.beansContainer.addMore(beans);
    }

    public CoffeeBeanWaste emptyWaste() {
        return dripper.emptyWaste();
    }

    public String status() {
        return "STATUS:" + '\n'
                + "Water: " + waterContainer.getQuantity() + "ml" + '\n'
                + "Milk: " + milkContainer.getQuantity() + "ml" + '\n'
                + "Beans: " + beansContainer.getQuantity() + "g" + '\n'
                + "Grounds: " + dripper.groundsQuantity() + "g" + '\n'
                + "Waste: " + (dripper.wastePresent() ? "y" : "n") + '\n';
    }

}
