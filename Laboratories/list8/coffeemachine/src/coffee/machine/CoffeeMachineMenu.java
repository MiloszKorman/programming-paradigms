package coffee.machine;

import coffee.machine.impl.*;
import coffee.machine.impl.exceptions.NotEnoughBeansException;
import coffee.machine.impl.exceptions.NotEnoughMilkException;
import coffee.machine.impl.exceptions.NotEnoughWaterException;
import coffee.machine.impl.exceptions.WasteException;
import shared.*;
import shared.items.Container;
import shared.products.*;

import java.util.Scanner;

public class CoffeeMachineMenu {

    private final String MENU =
            "1. Make coffee" + '\n' +
            "2. Refill water container" + '\n' +
            "3. Refill milk container" + '\n' +
            "4. Refill coffee beans container" + '\n' +
            "5. Clean up coffee bean waste" + '\n' +
            "6. Show status" + '\n' +
            "7. Turn the machine off" + '\n';
    private final String COFFEES =
            "1. Americano" + '\n' +
            "2. Latte" + '\n' +
            "3. Cappuccino" + '\n' +
            "4. Espresso" + '\n' +
            "5. Macchiato" + '\n';

    private final Scanner sc;

    private final CoffeeMachine coffeeMachine;
    /*private final Container<Water> waterContainer;
    private final Container<Milk> milkContainer;
    private final Container<CoffeeBeans> beansContainer;*/

    public CoffeeMachineMenu() {
        this.sc = new Scanner(System.in);
        this.coffeeMachine = new CoffeeMachine();
        /*this.waterContainer = new Container<>(new Water(10000));
        this.milkContainer = new Container<>(new Milk(5000));
        this.beansContainer = new Container<>(new CoffeeBeans(3000));*/
    }

    public void run() {
        boolean end = false;
        while (!end) {
            System.out.println(MENU);
            int choice = Tools.nextIntBetween(1, 7, sc);
            switch (choice) {
                case 1:
                    try {
                        Container<Coffee> coffeeMade = new Container<>(coffeeMachine.makeCoffee(chooseCoffeeKind()));
                        try {
                            coffeeMade.drink();
                        } catch (UndrinkableException e) {
                            System.out.println("Trying to drink undrinkable product (Somehow it's not coffee)");
                            e.printStackTrace();
                        }
                    } catch (NotEnoughBeansException beans) {
                        System.out.println("Not enough beans in the coffee machine");
                    } catch (NotEnoughMilkException milk) {
                        System.out.println("Not enough milk in the coffee machine");
                    } catch (NotEnoughWaterException water) {
                        System.out.println("Not enough water in the coffee machine");
                    } catch (WasteException waste) {
                        System.out.println("Waste present in the coffee machine, needs disposing");
                    }
                    break;
                case 2:
                    coffeeMachine.refillWater(new Water(1000));
                    break;
                case 3:
                    coffeeMachine.refillMilk(new Milk(1000));
                    break;
                case 4:
                    coffeeMachine.refillBeans(new CoffeeBeans(100));
                    break;
                case 5:
                    coffeeMachine.emptyWaste();
                    break;
                case 6:
                    System.out.println(coffeeMachine.status());
                    break;
                case 7:
                    System.out.println("Bye bye");
                    end = true;
                    break;
            }
        }
    }

    private CoffeeKind chooseCoffeeKind() {
        CoffeeKind coffeeChoice;

        System.out.println(COFFEES);
        int choice = Tools.nextIntBetween(1, 5, sc);
        switch (choice) {
            case 1:
                coffeeChoice = CoffeeKind.Americano;
                break;
            case 2:
                coffeeChoice = CoffeeKind.Latte;
                break;
            case 3:
                coffeeChoice = CoffeeKind.Cappuccino;
                break;
            case 4:
                coffeeChoice = CoffeeKind.Espresso;
                break;
            case 5:
                coffeeChoice = CoffeeKind.Macchiato;
                break;
            default:
                coffeeChoice = chooseCoffeeKind();
        }

        if (coffeeChoice.getMilkAmount() > 0) {
            if (chooseFoamed()) {
                coffeeChoice.foam();
            }
        }

        return coffeeChoice;
    }

    private boolean chooseFoamed() {
        System.out.print("Do you want the milk foamed? (y/n): ");
        return Tools.nextBoolean(sc);
    }
}
