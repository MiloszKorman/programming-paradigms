Dzisiejszym zadaniem jest zasymulowanie wyrywka świata zewnętrznego w programowaniu
obiektowym.

Proszę opracować kilka obiektów różnych klas.

Część klas powinna mieć wspólna klasę bazową

Powinny one implementować interfejsy (ok 3)

Pamiętajcie o zasadzie SOLID

W programie proszę o zaprezentowanie użycia wspomnianych klas z zastosowaniem interfejsów,
rzutowania.

Sugerowane wyrywki świata:

- Psi Patrol 
- Bajka Sing
- Zwierzęta
- Figury (kwadrat, prostokąt, itp.)
- Prowadzący Pwr 

Język: Java

Punktów: 10
