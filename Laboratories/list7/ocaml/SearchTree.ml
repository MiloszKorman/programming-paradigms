  type 'a bt = Leaf | Node of 'a * 'a bt * 'a bt
  type 'a t = { mutable root: int bt }
  exception NotFound of string
  let create() = { root = Leaf }

  let push (el, sT) = 
    let rec inPush = function
        Node(v, lt, rt) as tN -> 
          if el < v then Node(v, inPush lt, rt)
          else if el > v then Node (v, lt, inPush rt)
          else tN
      | Leaf -> Node(el, Leaf, Leaf)
    in sT.root <- inPush sT.root

  let remove (el, sT) =
    let rec inRemove iEl = function
        Node(v, lt, rt) as toRemove ->
          if iEl < v then Node(v, inRemove iEl lt, rt)
          else if iEl > v then Node(v, lt, inRemove iEl rt)
          else (match toRemove with
                 | Leaf -> failwith "Not happenin"
                 | Node(_, Leaf, Leaf) -> Leaf
                 | Node(_, lt, Leaf) -> lt
                 | Node(_, Leaf, rt) -> rt
                 | Node(_, lt, rt) -> 
                     let rec minValue = function
                         Leaf -> failwith "Not happenin"
                       | Node(min, Leaf, _) -> min
                       | Node(_, lST, _) -> minValue lST
                     in let suc = minValue rt 
                     in Node(suc, lt, inRemove suc rt)
               )
      | Leaf -> raise (NotFound "module SearchTree: remove")
    in sT.root <- inRemove el sT.root

  let find (el, sT) =
    let rec inFind = function
        Node(v, lt, rt) -> 
          if el < v then inFind lt
          else if el > v then inFind rt
          else true
      | Leaf -> false
    in inFind sT.root

  let getPreOrder sT = 
    let rec preorder = function
        Leaf -> []
      | Node(v, lt, rt) -> v::(preorder lt @ preorder rt)
    in preorder sT.root

  let getPostOrder sT = 
    let rec postorder = function
        Leaf -> []
      | Node(v, lt, rt) -> postorder lt @ postorder rt @ [v]
    in postorder sT.root

  let getInOrder sT =
    let rec inorder = function
        Leaf -> []
      | Node(v, lt, rt) -> inorder lt @ (v :: inorder rt)
    in inorder sT.root
