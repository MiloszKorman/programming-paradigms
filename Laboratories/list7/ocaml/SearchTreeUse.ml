(*#use "SearchTree.ml";;*)

let rec print_list = function
    h::t -> 
      print_int h;
      print_string " ";
      print_list t
  | [] -> ();;

let sT = SearchTree.create();;
SearchTree.push(4, sT);;
SearchTree.find(4, sT);;
SearchTree.find(2, sT);;
SearchTree.push(2, sT);;
SearchTree.push(6, sT);;
print_string "Add 4 2 6 to the tree\n";;
print_string "Preorder: ";;
print_list (SearchTree.getPreOrder sT);;
print_string "\n";;
print_string "Postorder: ";;
print_list (SearchTree.getPostOrder sT);;
print_string "\n";;
print_string "Inorder: ";;
print_list (SearchTree.getInOrder sT);;
print_string "\n";;
SearchTree.remove(4, sT);;
SearchTree.find(4, sT);;
SearchTree.find(2, sT);;
SearchTree.find(6, sT);;
print_string "Remove 4 from the tree\n";;
print_string "Preorder: ";;
print_list (SearchTree.getPreOrder sT);;
print_string "\n";;
print_string "Postorder: ";;
print_list (SearchTree.getPostOrder sT);;
print_string "\n";;
print_string "Inorder: ";;
print_list (SearchTree.getInOrder sT);;
