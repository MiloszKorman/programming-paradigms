## Zad 1. (OCaml)

Zdefiniuj funkcję filtrującą elementy z listy list. Funkcja ma mieć dwa parametry: listę list i wartość.
W liście wynikowej pozostać mają tylko te elementy, których każdy element listy jest różny od
podanej wartości.

### Przykład: [[1;2;3];[3;4];[5;6]] 3 -> [[5;6]]

## Zad 2. (OCaml lub Scala)

Zdefiniuj funkcję przekształcającą listę cyfr binarnych [0;1] w liczbę dziesiętną. Funkcja ma mieć jeden
parametr.

### Przykład [1;0;1] -> 5

## Zad 3. (OCaml lub Scala)

Zdefiniuj funkcję, która sprawdza czy 3 punkty na płaszczyźnie tworzą trójkąt.
