import scala.annotation.tailrec

def binaryToDecimal(bins: List[Int]): Int = {
  @tailrec
  def innerBinaryToDecimal(result: Int, bins: List[Int], currWeight: Int): Int = bins match {
    case Nil => result
    case hd::tl => innerBinaryToDecimal({
        if (hd == 0) 0
        else if (hd == 1) currWeight
        else throw new RuntimeException("Illegal parameter")
      } + result, tl, currWeight*2)
  }
  innerBinaryToDecimal(0, bins.reverse, 1)
}

binaryToDecimal(List(0, 0, 0)) //0
binaryToDecimal(List(0, 0, 1)) //1
binaryToDecimal(List(0, 1, 0)) //2
binaryToDecimal(List(1, 0, 1)) //5
binaryToDecimal(List(2, 0, 1)) //runtime exception

def binaryToDecimalOneParameterONLY(bins: List[Int]): Int = bins match {
  case Nil => 0
  case hd::tl => {
    if (hd == 0) 0
    else if (hd == 1) Math.pow(2, bins.length - 1).toInt
    else throw new RuntimeException("Illegal parameter")
  } + binaryToDecimalOneParameterONLY(tl)
}

binaryToDecimalOneParameterONLY(List(0, 0, 0)) //0
binaryToDecimalOneParameterONLY(List(0, 0, 1)) //1
binaryToDecimalOneParameterONLY(List(0, 1, 0)) //2
binaryToDecimalOneParameterONLY(List(1, 0, 1)) //5
binaryToDecimalOneParameterONLY(List(2, 0, 1)) //runtime exception

def binaryToDecimalCheeky(bins: List[Int]): Int =
  bins.foldRight(0, 1)((x, buf) => (buf._1 + x * buf._2, buf._2 * 2))._1

binaryToDecimalCheeky(List(0, 0, 0)) //0
binaryToDecimalCheeky(List(0, 0, 1)) //1
binaryToDecimalCheeky(List(0, 1, 0)) //2
0 + (2 * (1 + (2 * 0)))
binaryToDecimalCheeky(List(1, 0, 1)) //5