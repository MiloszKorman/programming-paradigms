def isTriangle(a: (Int, Int), b: (Int, Int), c: (Int, Int)): Boolean = {
  val triangle = !((c._2 - b._2) * (b._1 - a._1) == (b._2 - a._2) * (c._1 - b._1))
  val unique = !(((a._1 == b._1) && (a._2 == b._2)) || ((a._1 == c._1) && (a._2 == c._2)) || ((b._1 == c._1) && (b._2 == c._2)))
  triangle && unique
}

isTriangle((0, 0), (1, 1), (2, 2)) //false
isTriangle((0, 0), (1, 1), (2, 4)) //true
isTriangle((0, 1), (0, 3), (0, 4)) //false
isTriangle((1, 4), (1, 4), (2, 7)) //false
isTriangle((1, 4), (4, 1), (1, 7)) //true
isTriangle((1, 1), (2, 2), (3, 3)) //false