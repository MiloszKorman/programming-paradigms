let rec filter (xss, x) = 
  let rec isInside xs =
    match xs with
      [] -> false
    | hd::tl -> if hd = x then true else isInside tl
  in
  match xss with
    [] -> []
  | hd::tl -> if isInside hd then filter (tl, x) else hd::filter(tl, x);;

filter ([[1;2;3];[3;4];[5;6]], 3);; (* [[5;6]] *)
filter ([[1;2;3];[2;3];[4;6]], 1);; (* [[2;3];[4;6]] *)
filter ([[1;2;3];[2;3];[2;4]], 2);; (* [] *)