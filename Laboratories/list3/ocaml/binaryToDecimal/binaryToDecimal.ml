let binaryToDecimal bins =
  let rec innerBinaryToDecimal (result, bins, currWeight) =
    match bins with
      [] -> result
    | hd::tl -> innerBinaryToDecimal ((
        if hd = 0 then 0
        else if hd = 1 then currWeight
        else failwith "Illegal parameter"
      ) + result, tl, currWeight * 2)
  in
  innerBinaryToDecimal(0, List.rev bins, 1);;

binaryToDecimal(0::0::0::[]);; (* 0 *)
binaryToDecimal(0::0::1::[]);; (* 1 *)
binaryToDecimal(0::1::0::[]);; (* 2 *)
binaryToDecimal(1::0::1::[]);; (* 5 *)
binaryToDecimal(2::0::1::[]);; (* Exception: Failure Illegal parameter *)

let rec binaryToDecimalOneParameterONLY bins = 
  match bins with
    [] -> 0
  | hd::tl -> (
      if hd = 0 then 0
      else if hd = 1 then int_of_float (2.**(float_of_int ((List.length bins) - 1)))
      else failwith "Illegal parameter"
    ) + binaryToDecimalOneParameterONLY tl;;

binaryToDecimalOneParameterONLY(0::0::0::[]);; (* 0 *)
binaryToDecimalOneParameterONLY(0::0::1::[]);; (* 1 *)
binaryToDecimalOneParameterONLY(0::1::0::[]);; (* 2 *)
binaryToDecimalOneParameterONLY(1::0::1::[]);; (* 5 *)
binaryToDecimalOneParameterONLY(2::0::1::[]);; (* Exception: Failure Illegal parameter *)