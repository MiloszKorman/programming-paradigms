let rec findOddOne xs =
  match xs with
    [] -> 0
  | hd::tl -> (lxor) hd (findOddOne tl);;

findOddOne [1;2;3;4;2;3;2;1;2];;