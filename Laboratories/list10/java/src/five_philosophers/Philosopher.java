package five_philosophers;

import java.util.concurrent.Semaphore;

public class Philosopher extends Thread {
    private final Stick leftStick, rightStick;
    private final Semaphore doorman;

    public Philosopher(int n, Stick leftStick, Stick rightStick, Semaphore doorman) {
        super("Philosopher" + (n+1));
        this.leftStick = leftStick;
        this.rightStick = rightStick;
        this.doorman = doorman;
    }

    public void meditate() throws InterruptedException {
        System.out.println(getName() + " meditating");
        sleep(1000);
        System.out.println(getName() + " finished meditating");
        eat();
    }

    public void eat() throws InterruptedException {
        doorman.acquire();
        leftStick.take();
        rightStick.take();
        System.out.println(getName() + " eating");
        sleep(100);
        leftStick.give();
        rightStick.give();
        doorman.release();
        meditate();
    }

    @Override
    public void run() {
        try {
             meditate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
