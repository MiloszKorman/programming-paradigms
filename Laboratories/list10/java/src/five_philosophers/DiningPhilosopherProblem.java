package five_philosophers;

import java.util.concurrent.Semaphore;

public class DiningPhilosopherProblem {
    public static void main(String[] args) {
        final Philosopher[] philosophers = new Philosopher[5];
        final Stick[] sticks = new Stick[5];
        final Semaphore doorman = new Semaphore(4, true);

        for (int i = 0; i < 5; i++) {
            sticks[i] = new Stick();
        }
        for (int i = 0; i < 5; i++) {
            philosophers[i] = new Philosopher(i, sticks[i], sticks[(i+1)%5], doorman);
            philosophers[i].start();
        }
    }
}
