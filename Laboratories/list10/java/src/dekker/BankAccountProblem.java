package dekker;

public class BankAccountProblem {
    public static void main(String[] args) {
        BankAccount account = new BankAccount();
        Giver giver = new Giver(account);
        Taker taker = new Taker(account);
        giver.start();
        taker.start();

        try {
            giver.join();
            taker.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
