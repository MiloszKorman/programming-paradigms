package dekker;

import java.util.concurrent.Semaphore;

class BankAccount {
    private int balance;
    private boolean zeroed;
    private Semaphore lock;

    BankAccount() {
        this.balance = 1000;
        lock = new Semaphore(1);
    }

    synchronized void setBalance(int newBalance) {
        this.balance = newBalance;
        if (newBalance == 0) zeroed = true;
    }

    synchronized int getBalance() {
        return balance;
    }

    boolean wasZeroed() {
        return zeroed;
    }

    void lock() {
        try {
            lock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void unlock() {
        lock.release();
    }
}
