package bank_account;

class Taker extends Thread {

    private final BankAccount account;

    Taker(BankAccount bankAccount) {
        this.account = bankAccount;
    }

    @Override
    public void run() {
        while (!account.wasZeroed()) {
            int balance = account.getBalance();
            sleep(Math.random() * 20);
            account.setBalance(balance-100);
            System.out.println("Taker withdraws, now: " + account.getBalance());
            sleep(Math.random() * 40);
        }
    }

    private void sleep(double howLong) {
        try {
            Thread.sleep((long) howLong);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
