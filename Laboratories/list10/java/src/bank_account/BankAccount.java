package bank_account;

class BankAccount {
    private int balance;
    private boolean zeroed;

    BankAccount() {
        this.balance = 1000;
        this.zeroed = false;
    }

    void setBalance(int newBalance) {
        this.balance = newBalance;
        if (newBalance == 0) zeroed = true;
    }

    int getBalance() {
        return balance;
    }

    boolean wasZeroed() {
        return zeroed;
    }
}
