package bank_account;

class Giver extends Thread {

    private final BankAccount account;

    Giver(BankAccount bankAccount) {
        this.account = bankAccount;
    }

    @Override
    public void run() {
        while (!account.wasZeroed()) {
            int balance = account.getBalance();
            sleep(Math.random() * 30);
            account.setBalance(balance+100);
            System.out.println("Giver deposits, now: " + account.getBalance());
            sleep(Math.random() * 50);
        }
    }

    private void sleep(double howLong) {
        try {
            Thread.sleep((long) howLong);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
