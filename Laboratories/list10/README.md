Paradygmaty programowania

1.) Zaimplementuj program dwuwątkowy. W aplikacji istnieje współdzielony zasób "Stan konta bankowego".
Jeden wątek ma za zadanie wpłacać pieniądze na konto, a drugi wypłacać. Przyjmij, że:

- początkowy stan konta wynosi 1000 zł
- wątki wpłacają lub wypłacają stałą kwotę np. 100 zł.
- każdy z wątków zapamiętuje na jaką kwotę dokonał wypłat/wpłat
- wątek pobiera aktualny stan konta, odczekuje losowy czas (0-2 sekund) na przetworzenie operacji,
aktualizuje stan konta daną operacją, odczekuje losowy czas (2-4 sekund) na wykonanie kolejnej operacji

Wykonaj program nie używając żadnych metod rozwiązywania konfliktów i wypisuj kolejne stany konta po
operacjach.
Na koniec zestaw stan konta oczekiwany z faktycznie otrzymanym.
     Punkty: 2 (język Java)

2.) Zastosuj algorytm Dekkera do rozwiązania konfliktu w powyższym problemie
     Punkty: 2 (język Java)

3.) Zaimplementuj rozwiązanie problemu 5-ciu filozofów.
     Punkty: 3 (język Java)
