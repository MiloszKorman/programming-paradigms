def findOddOne(list: List[Int]): Int = list match {
  case Nil => 0
  case hd::Nil => hd
  case hd::tl => hd^findOddOne(tl)
}

findOddOne(List(1, 3, 2, 1, 5, 2, 2, 2, 3)) //5
findOddOne(List(1, 2, 3, 2, 1)) //3
findOddOne(List(1, 1, 1, 1, 2, 3, 1, 1, 3, 4, 4)) //2