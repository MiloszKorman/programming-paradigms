import scala.annotation.tailrec;

def fibonacciTailCall(n: Int): Int ={
  @tailrec
  def innerFibonacci(n: Int, pn: Int, ppn: Int): Int =
    if (n == 0) ppn
    else innerFibonacci(n-1, pn+ppn, pn)
  if (n < 0) -1
  else innerFibonacci(n, 1, 0)
}

fibonacciTailCall(-1)
fibonacciTailCall(0) //0
fibonacciTailCall(1) //1
fibonacciTailCall(2) //1
fibonacciTailCall(3) //2
fibonacciTailCall(4) //3
fibonacciTailCall(5) //5
fibonacciTailCall(6) //8
fibonacciTailCall(7) //13
fibonacciTailCall(8) //21
fibonacciTailCall(9) //34
fibonacciTailCall(10) //55

def fibonacci(n: Int): Int = {
  if (n == 0) 0
  else if (n == 1) 1
  else fibonacci(n-1) + fibonacci(n-2)
}

fibonacci(0) //0
fibonacci(1) //1
fibonacci(2) //1
fibonacci(3) //2
fibonacci(4) //3
fibonacci(5) //5
fibonacci(6) //8
fibonacci(7) //13
fibonacci(8) //21
fibonacci(9) //34
fibonacci(10) //55
