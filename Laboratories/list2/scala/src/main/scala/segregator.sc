import scala.annotation.tailrec;

def segregator(list: List[Int]): (List[Int], List[Int], List[Int]) = {
  @tailrec
  def innerSegregator(input: List[Int], result: (List[Int], List[Int], List[Int])): (List[Int], List[Int], List[Int]) =
    input match {
      case Nil => (result._1.reverse, result._2.reverse, result._3.reverse)
      case hd::tl =>
        if (hd%2==0) innerSegregator(tl, (hd::result._1, result._2, result._3))
        else if (hd > 0) innerSegregator(tl, (result._1, hd::result._2, result._3))
        else innerSegregator(tl, (result._1, result._2, hd::result._3))
    }
  innerSegregator(list, (List[Int](), List[Int](), List[Int]()))
}

segregator(List())
segregator(List(-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10))