import scala.annotation.tailrec;

def merger[A] (list1: List[A], list2: List[A]): List[A] = {
  @tailrec
  def innerMerger [A] (result: List[A], list1: List[A], list2: List[A]): List[A] = {
    (list1, list2) match {
      case (Nil, Nil) => result.reverse
      //case (hd::tl, Nil) => innerMerger(hd::result, tl, Nil)
      case (hd::tl, otherOne) => innerMerger(hd::result, otherOne, tl)
      case (Nil, otherOne) => innerMerger(result, otherOne, Nil)
    }
  }
  innerMerger(Nil, list1, list2)
}

merger(List(1,3,5), List(2,4,6)) // 1,2,3,4,5,6
merger(List(1,3,5,7,9,11,13), List(2,4,6))
merger(List(1,3,5), List(2,4,6,8,10,12))
merger(List(1, 2, 3), Nil)
merger(Nil, List(4,5,6))
merger(Nil, Nil)