## Zad 1. (OCaml)

Napisać funkcję anonimową sprawdzającą czy dana liczba jest podzielna przez 7.

## Zad 2. (Scala)

Napisać funkcję zwracającą N-ty wyraz ciągu Fibonacciego. Należy użyć rekurencji ogonowej

## Zad 3. (OCaml lub Scala)

Napisz funkcję rozdzielającą listy wejściowe na 3 podlisty. W pierwszej podliście mają się znaleźć
wszystkie elementy parzyste. W drugiej podliście mają się znaleźć wszystkie elementy o wartościach
nieparzystych dodatnich. W trzeciej nieparzyste ujemne.

### Porządek elementów musi być zachowany. Wynik zwrócić w postaci pary list.

## Zad 4. (OCaml lub Scala)

Napisz funkcję łączącą dwie podane listy. Elementy w liście wyjściowej mają występować
naprzemiennie.

## Zad 5. (OCaml lub Scala)

Dostajesz dużą listę liczb, każda liczba występuje w tej liście parzystą liczbę razy, znajdź jedyną liczbę, która występuje nieparzystą liczbę razy

# UWAGA: Każde zadanie ma zawierać minimalną liczbę testów, które sprawdzą jej poprawne działanie.

