let rec filter(xss, x) = 
  let rec filterSingleList xs =
    match xs with
      [] -> []
    | x::tl -> filterSingleList(tl)
    | hd::tl -> hd::filterSingleList(tl)
  in
  match xss with
    [] -> []
  | hd::tl -> filterSingleList(hd)@filter(tl, x);;

filter([[1;2;3];[3;4];[5;6]], 3);;