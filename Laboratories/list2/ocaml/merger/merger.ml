let merger (list1, list2) = 
  let rec innerMerger(result, list1, list2) = 
    match (list1, list2) with
      ([], []) -> (List.rev result)
    (*| (hd::tl, []) -> innerMerger(hd::result, tl, [])*)
    | (hd::tl, otherOne) -> innerMerger(hd::result, otherOne, tl)
    | ([], otherOne) -> innerMerger(result, otherOne, [])
  in
  innerMerger([], list1, list2);;

merger([1;3;5], [2;4;6]);; (* 1,2,3,4,5,6 *)
merger([1;3;5;7;9;11;13], [2;4;6]);;
merger([1;3;5], [2;4;6;8;10;12]);;
merger([1; 2; 3], []);;
merger([], [4;5;6]);;
merger([], []);;
