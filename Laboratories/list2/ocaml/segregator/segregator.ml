let segregator list = 
  let rec innerSegregator(input, resultEven, resultOddPlus, resultOddMinus) = 
    match input with
      [] -> ((List.rev resultEven), (List.rev resultOddPlus), (List.rev resultOddMinus))
    | hd::tl -> 
      if (hd mod 2 = 0) then innerSegregator(tl, hd::resultEven, resultOddPlus, resultOddMinus) 
      else if (hd > 0) then innerSegregator(tl, resultEven, hd::resultOddPlus, resultOddMinus)
      else innerSegregator(tl, resultEven, resultOddPlus, hd::resultOddMinus)
  in
  innerSegregator(list, [], [], []);;

segregator([]);;
segregator([-3;-2;-1;0;1;2;3;4;5;6;7;8;9;10]);;