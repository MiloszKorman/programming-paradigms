WAŻNE1: Do każdego zadania przygotuj 3-5 testów sprawdzających poprawność działania!

WAŻNE2: Wolno używać tylko funkcji bibliotecznych o złożoności obliczeniowej O(1).

1) Napisz funkcje wkładającą element do posortowanej kolekcji, w taki sposób, aby wyjściowa kolekcja
pozostawała posortowana.
Funkcję napisz w języku Ocaml w sposób funkcyjny przy użyciu list oraz w języku Java w sposób imperatywny
przy użyciu tablic.
przykład: insert [1;3;5;7] 4 daje wynik [1;3;4;5;7]
Punkty: 4 (język Ocaml i Java)

2) Zdefiniuj funkcję "lpowiel" i "powiel" powielającą elementy w leniwej/gorliwej liście liczb, tyle razy ile
wynosi wartość aktualnej liczby.
Przykład: [1;2;3] daje [1;2;2;3;3;3]
Wyniki powinny być zapisane w postaci leniwej/gorliwej!
Punkty: 3 (język Ocaml)

