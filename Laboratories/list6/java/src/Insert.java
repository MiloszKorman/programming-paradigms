import java.lang.reflect.Array;
import java.util.Arrays;

public class Insert {

    public static void main(String[] args) {
        Integer a[] = {1,3,5,7};
        System.out.println(Arrays.toString(insert(a, 4))); //[1,3,4,5,7]
        System.out.println(Arrays.toString(insert(a, 0))); //[0,1,3,5,7]
        System.out.println(Arrays.toString(insert(a, 8))); //[1,3,5,7,8]
        System.out.println(Arrays.toString(insert(a, 3))); //[1,3,3,5,7]
    }

    @SuppressWarnings("unchecked")
    static <T extends Comparable> T[] insert(T[] tab, T toIns) {
        T[] toReturn = (T[]) Array.newInstance(tab.getClass().getComponentType(), tab.length + 1);

        int i = 0;
        for (; i < tab.length && tab[i].compareTo(toIns) < 0; i++)
            toReturn[i] = tab[i];

        toReturn[i] = toIns;

        for (; i < tab.length; i++)
            toReturn[i+1] = tab[i];

        return toReturn;
    }

}
