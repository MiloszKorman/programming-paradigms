package paws

class Paw(private val r: String, private val h: String) {

  def race: String =  r

  def vehicle: () => String = () => h
}
