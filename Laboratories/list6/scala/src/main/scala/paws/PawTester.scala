package paws

object PawTester {
  def main(args: Array[String]): Unit = {
    val marshall = new Paw("dalmatian", "fire engine")
    println(marshall.race)
    println(marshall.vehicle())
  }
}
