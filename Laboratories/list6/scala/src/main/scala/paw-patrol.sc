sealed trait Paws
case class Paw(race: String, vehicle: () => String) extends Paws

val marshall = Paw("dalmatian", () => "fire engine")
marshall.race
marshall.vehicle()
