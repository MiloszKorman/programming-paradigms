type 'a llist = LKoniec | LElement of 'a * (unit -> 'a llist);;

let lpowiel = function
    LKoniec -> LKoniec
  | LElement(v, nl) as el -> 
      let rec inLPowiel = function 
          (LKoniec, _) -> LKoniec
        | (LElement(_, nl), 0) -> (
            match nl() with
                LKoniec -> LKoniec
              | LElement(v, nnl) as ll -> inLPowiel(ll, v)
          )
        | (LElement(v, nl) as ll, n) -> LElement(v, fun () -> inLPowiel (ll, n-1))
      in inLPowiel(el, v);;

let rec ltake = function
    (0, _) -> []
  | (_, LKoniec) -> []
  | (n, LElement(x, xf)) -> x::ltake(n-1, xf());;

let rec lfrom k = LElement (k, fun () -> lfrom (k+1));;

ltake (3, lpowiel (LElement(0, fun () -> LElement(1, fun () -> LElement(2, fun () -> LKoniec)))));;
ltake (10, lpowiel (lfrom 1));;
