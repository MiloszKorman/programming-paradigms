let powiel l =
  let rec inPowiel = function
      ([], _) -> []
    | (_::t, 0) -> if t = [] then [] else inPowiel (t, List.hd t)
    | (h::t as li, n) -> h::inPowiel (li, n-1)
  in if l = [] then [] else inPowiel(l, List.hd l);;

powiel [1;2;3];; (* [1;2;2;3;3;3] *)
powiel [0;1;2];; (* [1;2;2] *)
powiel [];; (* [] *)
