let rec insert xs x =
  match xs with
      [] -> [x]
    | h::t as l -> 
        if h < x then h::insert t x
        else x::l;;

let list = [1;3;5;7];;
insert list 4;; (* [1;3;4;5;7] *)
insert list 0;; (* [0;1;3;5;7] *)
insert list 8;; (* [1;3;5;7;8] *)
insert list 3;; (* [1;3;3;5;7] *)

