type race = string;;
type vehicle = string;;
type paw = Paw of race * (unit -> vehicle);;

let marshall = Paw ("dalmatian", fun () -> "fire engine");;
let rubble = Paw ("English bulldog", fun () -> "bulldozer");;
let chase = Paw ("German Shepar", fun () -> "cruiser");;
let rocky = Paw ("mongrel", fun () -> "recycling truck");;

let Paw (mR, mV) = marshall;;
mR;;
mV();;
