let sumProd xs = List.fold_left (fun acc x -> (x + (fst acc), x * (snd acc))) (0, 1) xs;;

sumProd [1;2;3;4;5;6];; (* (21, 720) *)