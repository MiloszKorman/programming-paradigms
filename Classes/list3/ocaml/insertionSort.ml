let insertionSort comparison xs =
  let rec addInProperPlace x = function
      [] -> [x]
    | hd::tl as list -> if comparison hd x then hd::addInProperPlace x tl else x::list
  in
    List.fold_left (fun sorted x -> addInProperPlace x sorted) [] xs;;

(*
insertionSort (fun x y -> x <= y) [5;2;1;6;7;3;2;1;4;4;1;6;4;3];;
insertionSort (fun x y -> String.length x <= String.length y) ["Boniek";"Jan";"Darek"];;*)

insertionSort (fun (x1, y1) (x2, y2) -> x1 <= x2) [(1,3);(1,1);(1,2)];;
insertionSort (fun x1 x2 -> x1 <= x2) [4;2;1;4;5;6;7;2;4;3;1];;
