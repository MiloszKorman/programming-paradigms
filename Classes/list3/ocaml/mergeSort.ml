let rec mergesort comparison = function
    [] -> []
  | [x] -> [x]
  | xs -> 
      let rec halve = function
          ([], _) -> ([], [])
        | (l, 0) -> ([], l)
        | (h::t, n) -> let (nl, nr) = halve (t, n-1) in (h::nl, nr)
      and connect = function
          (ys, []) -> ys
        | ([], zs) -> zs
        | ((y::ys as fys), (z::zs as fzs)) -> 
            if comparison y z then y::connect (ys, fzs)
            else z::connect (fys, zs)
      in let(lH, rH) = halve (xs, (List.length xs)/2) in connect (mergesort comparison lH, mergesort comparison rH);;

(*
mergesort (fun x y -> x <= y) [5;3;1;2;5;4;2;1;2;5;7;7;9;5];;
mergesort (fun x y -> x <= y) [];;
mergesort (fun x y -> x <= y) [1];;
mergesort (fun x y -> x <= y) [3;1;2];;
*)

mergesort (fun (x1,y1) (x2,y2) -> x1 <= x2) [(1,3);(1,1);(1,2)];;
