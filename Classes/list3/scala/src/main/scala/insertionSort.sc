def insertionSort [A] (comparison: A=>A=>Boolean) = (xs: List[A]) => {
  def addInProperPlace(x: A, ordered: List[A]): List[A] = ordered match {
    case Nil => x::Nil
    case hd::tl => if (!comparison(hd)(x)) x::ordered else hd::addInProperPlace(x, tl)
  }
  xs.foldLeft(List[A]())((sorted: List[A], x: A) => addInProperPlace(x, sorted))
}

insertionSort((x: Int) => (y: Int) => x <= y)(5::2::1::5::7::3::2::1::4::4::1::6::4::3::Nil)

insertionSort((x: String) => (y:String) => x.length <= y.length)("Boniek"::"Jan"::"Darek"::Nil)

insertionSort((xs: (Int, Int)) => (ys: (Int, Int)) => xs._1 <= ys._1)(List[(Int, Int)]((1,3), (1,1), (1,2)))
insertionSort((a: Int) => (b: Int) => a <= b)(List(4, 2, 1, 3, 5, 2, 1, 4, 7))