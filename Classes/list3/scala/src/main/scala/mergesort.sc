def mergeSort[A](comparison: A => A => Boolean)(list: List[A]): List[A] = list match {
  case Nil => Nil
  case x :: Nil => List(x)
  case xs =>
    def connect(xs: List[A])(ys: List[A]): List[A] = (xs, ys) match {
      case (xs, Nil) => xs
      case (Nil, ys) => ys
      case (xh :: xt, yh :: yt) =>
        if (comparison(xh)(yh)) xh :: connect(xt)(ys)
        else yh :: connect(xs)(yt)
    }
    val (lH, rH) = xs.splitAt(xs.length / 2)
    connect(mergeSort(comparison)(lH))(mergeSort(comparison)(rH))
}

mergeSort((x: Int) => (y: Int) => x <= y)(List(5, 2, 1, 3, 2, 2, 4, 1, 5))
mergeSort((xs: (Int, Int)) => (ys: (Int, Int)) => xs._1 <= ys._1)(List[(Int, Int)]((1,3), (1,1), (1,2)))