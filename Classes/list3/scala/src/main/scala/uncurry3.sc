def uncurry3 [A, B, C, F] (f: A => B => C => F) = (a: A, b: B, c: C) =>
  f(a)(b)(c)

def add3 (a: Int)(b: Int)(c: Int): Int = a + b + c

uncurry3(add3)(1, 2, 3)