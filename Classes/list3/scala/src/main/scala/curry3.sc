def curry3 [A, B, C, F] (f:(A, B, C)=>F) = (a: A) => (b: B) => (c: C) =>
  f(a, b, c)

def add3 (a: Int, b: Int, c: Int): Int = a + b + c

curry3(add3)(1)(2)(3)
val eins = curry3(add3)(1)
val zwei = eins(2)
zwei(3)
