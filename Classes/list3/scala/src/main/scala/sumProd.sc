def sumProd(xs: List[Int]): (Int, Int) = xs.foldLeft(0,1)((acc: (Int, Int), x: Int) => (acc._1 + x, acc._2 * x))

sumProd(List(1, 2, 3, 4, 5, 6)) //(21, 720)