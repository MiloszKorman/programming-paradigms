public class QueueTest {

    public static void main(String[] args) {
        MyQueue<String> queue = new MyQueueImpl<>(3);
        queue.enqueue("kot");
        queue.enqueue("pies");
        queue.enqueue("krowa");
        System.out.println(queue.first());
        queue.dequeue();
        queue.enqueue("owca");
        System.out.println(queue.first());
        queue.dequeue();
        queue.enqueue("kangur");
        System.out.println(queue.first());
        queue.enqueue("cokolwiek");
    }
}
