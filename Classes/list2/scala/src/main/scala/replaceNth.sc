def replaceNth [A] (list: List[A], n: Int, rep: A): List[A] = (n, list) match {
  case(_, Nil) => Nil
  case (0, hd::tl) => rep::tl
  case (_, hd::tl) => hd::replaceNth(tl, n-1, rep)
}

replaceNth('o'::'l'::'a'::Nil, 1, 's')

replaceNth('o'::'l'::'a'::Nil, 6, 's')

//b.) slajd 39