def root3(a: Double): Double = {
  def rootInside(a: Double, xi: Double, precision: Double): Double = {
    val end = Math.abs(Math.pow(xi, 3) - a) <= (precision * Math.abs(a))
    if (end) xi else rootInside(a, xi + ((a/Math.pow(xi, 2)) - xi)/3, precision)
  }
  val initial = if (a <= 1) a else  a/3
  rootInside(a, initial, Math.pow(10, -15))
}

root3(9)
