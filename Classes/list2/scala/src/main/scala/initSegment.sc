def initSegment[A] (segment: List[A], list: List[A]): Boolean = (segment, list) match {
  //case (Nil, Nil) => true
  case (Nil, _) => true
  case (_, Nil) => false
  case (hS::tS, hL::tL) => if (hS == hL) initSegment(tS, tL) else false
}

initSegment(Nil, List(1, 2, 3)) //true
initSegment(List(1, 2, 3), Nil) //false
initSegment(List(1), List(1, 2)) //true
initSegment(List(1, 2, 3), List(1)) //false
initSegment(List(1, 2, 3), List(1, 2, 3)) //true
initSegment(List(1, 3), List(1, 2, 3)) //false