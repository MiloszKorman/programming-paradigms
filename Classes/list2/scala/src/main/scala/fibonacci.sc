def fibonacci(n: Int): Int = n match {
  case 0 => 0
  case 1 => 1
  case _ => fibonacci(n - 1) + fibonacci(n - 2)
}

fibonacci(0)
fibonacci(1)
fibonacci(2)
fibonacci(3)
fibonacci(4)
fibonacci(5)
fibonacci(6)
fibonacci(7)
fibonacci(8)
fibonacci(9)
fibonacci(10)

def fibonacciTailCall(n: Int): Int = {
  def fibi(n: Int, pn: Int, ppn: Int): Int = n match {
    case 0 => ppn
    case _ => fibi(n-1, pn+ppn, pn)
  }
  fibi(n, 1, 0)
}

fibonacciTailCall(0)
fibonacciTailCall(1)
fibonacciTailCall(2)
fibonacciTailCall(3)
fibonacciTailCall(4)
fibonacciTailCall(5)
fibonacciTailCall(6)
fibonacciTailCall(7)
fibonacciTailCall(8)
fibonacciTailCall(9)
fibonacciTailCall(10)