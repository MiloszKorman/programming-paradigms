let rec fibonacci n = 
  match n with
    (0) -> 0
  | (1) -> 1
  | (_) -> fibonacci(n - 1) + fibonacci(n - 2);;

print_int (fibonacci 10);;

print_string "\n\n";;

let fibonacciTailCall n =
  let rec fibi(n, pn, ppn) =
    match n with
      0 -> ppn
    | _ -> fibi(n-1, pn+ppn, pn)
  in fibi(n, 1, 0);;

print_int (fibonacciTailCall 10);;