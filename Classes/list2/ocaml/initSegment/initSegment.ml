let rec initSegment(segment, list) = 
  match (segment, list) with
    ([], _ ) -> true
  | ( _ , []) -> false
  | (hS::tS, hL::tL) -> if (hS = hL) then initSegment(tS, tL) else false;;

print_string (string_of_bool (initSegment([], 1::2::3::[])));; (* true *)
print_string "\n";;
print_string (string_of_bool (initSegment(1::2::3::[], [])));; (* false *)
print_string "\n";;
print_string (string_of_bool (initSegment(1::[], 1::2::[])));; (* true *)
print_string "\n";;
print_string (string_of_bool (initSegment(1::2::3::[], 1::[])));; (* false *)
print_string "\n";;
print_string (string_of_bool (initSegment(1::2::3::[], 1::2::3::[])));; (* true *)
print_string "\n";;
print_string (string_of_bool (initSegment(1::3::[], 1::2::3::[])));; (* false *)