let root3 a = 
  let rec rootInside(a, xi, precision) =
    let ending = (abs_float(xi**3. -. a) <= (precision *. abs_float(a)))
    in 
    if(ending) then xi 
    else rootInside(a, xi +. (a /. (xi**2.) -. xi) /. 3., precision)
  in 
  let initial = if (a <= 1.) then a else a /. 3. in
  rootInside(a, initial, 10.**(-15.));;

print_float(root3(9.))