let rec replaceNth (list, n, rep) = 
  match (n, list) with
    ( _ , []) -> []
  | (0, hd::tl) -> rep::tl
  | ( _ , hd::tl) -> hd::replaceNth(tl, n-1, rep);;

let rec print_list = function 
    [] -> ()
  | e::l -> print_char e ; print_string " " ; print_list l;;

print_list(replaceNth('o'::'l'::'a'::[], 1, 's'));;