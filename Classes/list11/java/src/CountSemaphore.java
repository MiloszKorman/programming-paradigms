import java.util.concurrent.Semaphore;

class IntCellSemaphore {
    private int n = 0;
    public synchronized int getN() {
        return n;
    }
    public void setN(int n) {
        this.n = n;
    }
}

class CountSemaphore extends Thread {
    static IntCellSemaphore n = new IntCellSemaphore();
    private static Semaphore semaphore = new Semaphore(1);

    @Override public void run() {
        int temp;
        for (int i = 0; i < 200000; i++) {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            temp = n.getN();
            n.setN(temp + 1);
            semaphore.release();
        }
    }

    public static void main(String[] args) {
        CountSemaphore p = new CountSemaphore();
        CountSemaphore q = new CountSemaphore();
        p.start();
        q.start();
        try { p.join(); q.join(); }
        catch (InterruptedException e) { }
        System.out.println("The value of n is " + n.getN());
    }
}