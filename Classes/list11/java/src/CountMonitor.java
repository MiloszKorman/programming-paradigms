class IntCellMonitor {
    private int n = 0;
    private boolean changed = true;
    public synchronized int getN() {
        while (!changed) doWaiting();
        changed = false;
        notifyAll();
        return n;
    }
    public synchronized void setN(int n) {
        while (changed) doWaiting();
        this.n = n;
        changed = true;
        notifyAll();
    }
    private void doWaiting() {
        try {wait();}
        catch (InterruptedException e) {e.printStackTrace();}
    }
}

class CountMonitor extends Thread {
    static IntCellMonitor n = new IntCellMonitor();

    @Override public void run() {
        int temp;
        for (int i = 0; i < 200000; i++) {
            temp = n.getN();
            n.setN(temp + 1);
        }
    }

    public static void main(String[] args) {
        CountMonitor p = new CountMonitor();
        CountMonitor q = new CountMonitor();
        p.start();
        q.start();
        try { p.join(); q.join(); }
        catch (InterruptedException e) { }
        System.out.println("The value of n is " + n.getN());
    }
}