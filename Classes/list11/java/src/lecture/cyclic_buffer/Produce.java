package lecture.cyclic_buffer;

public interface Produce {
    public void put(int val);
}
