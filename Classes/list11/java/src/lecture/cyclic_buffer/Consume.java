package lecture.cyclic_buffer;

public interface Consume {
    public int take();
}
