package lecture;

public class ForkJoin {
    public static void main(String[] args) throws InterruptedException {
        Thread[] threads = new Thread[3];
        for (int i = 0; i < threads.length; i++) {
            final String message = "Thread " + i + " forked";
            threads[i] = new Thread(() -> {
                System.out.println(message);
            });
        }
        for (Thread t: threads) {
            t.start();
        }
        for (Thread t: threads) {
            t.join();
        }
        System.out.println("All threads joined");
    }
}
