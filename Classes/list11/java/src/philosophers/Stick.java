package philosophers;

import java.util.concurrent.Semaphore;

public class Stick {
    private final Semaphore rent = new Semaphore(1);

    public void take() throws InterruptedException {
        rent.acquire();
    }

    public void give() {
        rent.release();
    }
}
