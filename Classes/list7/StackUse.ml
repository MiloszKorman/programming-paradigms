#use "StackInterface.ml";;
#use "MyStack.ml";;

let s = MyStack.push(2, MyStack.push(1, MyStack.create()));;

let s2 = MyStack.push(3, s);;
MyStack.top s2;;
MyStack.top (MyStack.pop (MyStack.pop s));;
