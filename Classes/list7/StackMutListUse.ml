#use "StackMutList.ml";;

let s = StackMutList.create();;
StackMutList.push(1, s);;
StackMutList.push(2, s);;
StackMutList.top s;;
StackMutList.pop s;;
StackMutList.pop s;;
StackMutList.top s;;

