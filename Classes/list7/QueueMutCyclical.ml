#use "QueueMut.ml";;

module QueueMutCyclical : QUEUE_MUT = 
struct
  type 'a t = { mutable f: int; mutable r: int; size: int; mutable a: 'a option array }
  exception Empty of string
  exception Full of string
  let empty n = { f = 0; r = 0; size = n+1; a = Array.make (n+1) None }
  let enqueue (el, arr) =
    if (succ arr.r) mod arr.size = arr.f then raise (Full "module QueueMutCyclical: enqueue")
    else arr.a.(arr.r) <- Some el; arr.r <- (succ arr.r) mod arr.size
  let dequeue arr =
    if arr.r != arr.f then arr.f <- (succ arr.f) mod arr.size
  let first arr =
    if arr.f = arr.r then raise (Empty "module QueueMutCyclical: first")
    else match arr.a.(arr.f) with
        Some e -> e
      | None -> failwith "Should never come here"
  let isEmpty arr = arr.f = arr.r
  let isFull arr = (succ arr.r) mod arr.size = arr.f
end;;

let q = QueueMutCyclical.empty 4;;
QueueMutCyclical.enqueue(1, q);;
QueueMutCyclical.enqueue(2, q);;
QueueMutCyclical.enqueue(3, q);;
QueueMutCyclical.enqueue(4, q);;
QueueMutCyclical.isFull q;;
QueueMutCyclical.first q;;
QueueMutCyclical.dequeue q;;
QueueMutCyclical.first q;;
QueueMutCyclical.dequeue q;;
QueueMutCyclical.first q;;
QueueMutCyclical.dequeue q;;
QueueMutCyclical.first q;;
QueueMutCyclical.dequeue q;;
QueueMutCyclical.isEmpty q;;
