#use "QueueFun.ml";;
module QueueListTuple : QUEUE_FUN = 
struct
  type 'a t = 'a list * 'a list
  exception Empty of string
  let empty() = ([], [])
  let enqueue (el, l) = 
    match l with 
        ([], _) -> ([el], [])
      | (ll, rl) -> (ll, el::rl)
  let dequeue = function
      ([], []) -> ([], [])
    | ([x], rl) -> (List.rev rl, [])
    | (lh::lt, rl) -> (lt, rl)
    | ([], rl) -> failwith "Should nevere come here"
  let first = function
      ([], _) -> raise (Empty "module QueueListTuple: first")
    | (lh::lt, _) -> lh
  let isEmpty l = l = ([], [])
end;;

let q = QueueListTuple.empty();;
let q1 = QueueListTuple.enqueue(1, q);;
let q2 = QueueListTuple.enqueue(2, q1);;
QueueListTuple.first(q2);;
QueueListTuple.dequeue q2;;
QueueListTuple.first(QueueListTuple.dequeue q2);;
QueueListTuple.isEmpty q2;;
QueueListTuple.isEmpty(QueueListTuple.dequeue(QueueListTuple.dequeue q2));;
QueueListTuple.first(QueueListTuple.dequeue(QueueListTuple.dequeue q2));;

