type 'a stack = EmptyStack | Push of 'a * 'a stack;;

exception Empty of string;;

let empty() = EmptyStack;;

let push (e, s) = Push(e, s);;

let isEmpty = function
    EmptyStack -> true
  | Push _ -> false;;

let pop = function
    Push(_, s) -> s
  | EmptyStack -> EmptyStack;;

let top = function
    Push(e, _) -> e
  | EmptyStack -> raise (Empty "Stack: top");;

let s = Push(3, Push(2, Push(1, EmptyStack)));;

pop s;;

top s;;

