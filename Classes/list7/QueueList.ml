#use "QueueFun.ml";;

module QueueList : QUEUE_FUN = 
struct
  type 'a t = 'a list
  exception Empty of string
  let empty() = []
  let rec enqueue (el, l) = 
    match l with
        h::t -> h::enqueue(el, t)
      | [] -> [el]
  let dequeue = function
      _::t -> t
    | [] -> []
  let first = function
      h::_ -> h
    | [] -> raise (Empty "module QueueList: first")
  let isEmpty l = l = []
end;;

let q = QueueList.empty();;
let q1 = QueueList.enqueue(1, q);;
let q2 = QueueList.enqueue(2, q1);;
QueueList.first(q2);;
QueueList.dequeue q2;;
QueueList.first(QueueList.dequeue q2);;
QueueList.isEmpty q2;;
QueueList.isEmpty(QueueList.dequeue(QueueList.dequeue q2));;
QueueList.first(QueueList.dequeue(QueueList.dequeue q2));;
