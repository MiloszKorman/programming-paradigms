def swap [A] (tab: Array[A])(i: Int)(j:Int): Unit = {
  val aux = tab(i)
  tab(i) = tab(j)
  tab(j) = aux
}

def partition[A: Ordering] (tab: Array[A])(l: Int)(r: Int): (Int, Int) = {
  var i = l
  var j = r
  val pivot = tab((l+r)/2)
  while (i <= j) {
    while (implicitly[Ordering[A]].compare(tab(i), pivot) < 0) i += 1 //TODO potential mistake
    while (implicitly[Ordering[A]].compare(pivot, tab(j)) < 0) j-= 1 //TODO potential mistake
    if (i <= j) {
      swap(tab)(i)(j)
      i += 1
      j -= 1
    }
  }
  (i, j)
}

def quick [A: Ordering] (tab: Array[A])(l: Int)(r: Int): Unit = {
  if (l < r) {
    val (i, j) = partition(tab)(l)(r)
    if (j - l < r - i) {
      val _ = quick(tab)(l)(j)
      quick(tab)(i)(r)
    } else {
      val _ = quick(tab)(i)(r)
      quick(tab)(l)(j)
    }
  }
}

def quicksort[A: Ordering] (tab: Array[A]): Unit = quick(tab)(0)(tab.length - 1)

val t1 = Array(4,8,1,12,7,3,1,9)
quicksort(t1)
t1

val t2 = Array("kobyla","ma","maly","bok")
quicksort(t2)
t2
