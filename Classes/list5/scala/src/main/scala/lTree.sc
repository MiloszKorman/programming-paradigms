sealed trait lBT [+A]
case object LEmpty extends  lBT[Nothing]
case class LNode[+A] (elem: A, left: () => lBT[A], right: () => lBT[A]) extends lBT[A]

def lTree (n: Int): lBT[Int] = {
  LNode(n, () => lTree(2*n), () => lTree(2*n + 1))
}

def breadthBT [A] (root: lBT[A]): Stream[A] = {
  def inBreadthBT (buff: List[lBT[A]]): Stream[A] = buff match {
    case Nil => Stream.empty
    case LEmpty::lts => inBreadthBT(lts)
    case LNode(v, llt, lrt)::lts => v#::inBreadthBT(lts++List(llt(),lrt()))
  }
  inBreadthBT(List(root))
}

breadthBT(lTree(1)).take(10).force
