def lrepeat [A] (k: Int): Stream[A] => Stream[A] = (lxs: Stream[A]) => {
  def inLRepeat (n: Int): Stream[A] => Stream[A] = (ll: Stream[A]) => (n, ll) match {
    case (_, Stream.Empty) => Stream.empty
    case (0, x#::xs) => inLRepeat (k) (xs)
    case (n, original@(x#::xs)) => x#::(inLRepeat(n-1)(original))
  }
  inLRepeat(k)(lxs)
}

lrepeat(3)(Stream.from(1)).take(10).force