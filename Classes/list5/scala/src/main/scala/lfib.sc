val lfib: Stream[Int] = {
  def inLfib(pp: Int, p: Int): Stream[Int] = {
    (pp+p)#::inLfib(p, pp+p)
  }
  0#::1#::inLfib(0, 1)
}

lfib.take(10).force