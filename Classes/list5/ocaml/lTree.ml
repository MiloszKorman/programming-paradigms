type 'a lBT = LEmpty | LNode of 'a * (unit -> 'a lBT) * (unit -> 'a lBT);;

#use "lectureLazyList.ml";;

let rec lTree n =
  LNode (n, (fun () -> lTree (2*n)), (fun () -> lTree (2*n + 1)));;

let rec breadthBT root =
  let rec inBreadthBT = function
      [] -> LNil
    | LEmpty::lts -> inBreadthBT lts
    | LNode(v, llt, lrt)::lts -> LCons (v, fun () -> inBreadthBT (lts@[llt();lrt()]))
  in inBreadthBT (root::[]);;

ltake (10, (breadthBT (lTree 1)))
