type 'a llist = LNil | LCons of 'a * (unit -> 'a llist);;

let lhd = function
    LNil -> failwith "lhd"
  | LCons(x, _) -> x;;

lhd (LCons (1, (fun unit -> LNil)));;

let ltl = function
    LNil -> failwith "ltl"
  | LCons(_, xf) -> xf();;

lhd (ltl (LCons(1, (fun unit -> LCons(2, (fun unit -> LNil))))));;

let rec lfrom k = LCons (k, function () -> lfrom (k+1));;

let rec ltake = function
    (0, _) -> []
  | (_, LNil) -> []
  | (n, LCons(x, xf)) -> x::ltake(n-1, xf());;

let rec toLazyList = function
    [] -> LNil
  | hd::tl -> LCons(hd, function () -> toLazyList tl);;

ltake (3, toLazyList [1;2;3]);;

let rec (@$) ll1 ll2 =
  match ll1 with
      LNil -> ll2
    | LCons(x, xf) -> LCons(x, function () -> (xf()) @$ ll2);;

ltake (10, LCons(1, function () -> LNil) @$ lfrom 10);;

let rec lmap f = function
    LNil -> LNil
  | LCons(x, xf) -> LCons(f x, function () -> lmap f (xf()));;

ltake (10, lmap (function x -> x * 2) (lfrom 10));;

let rec sqr_llist = lmap (function x -> x*x);;

ltake (10, sqr_llist (lfrom 10));;

let rec lfilter pred = function
    LNil -> LNil
  | LCons(x, xf) -> if pred x
      then LCons(x, function () -> lfilter pred (xf()))
      else lfilter pred (xf());;

let rec liter f x = LCons(x, function () -> liter f (f x));;

ltake (10, liter (fun x -> (x+2)*3) 10);;

let primes =
  let rec sieve = function
      LNil -> failwith "Impossible, infinite list"
    | LCons(p, nf) -> LCons(p, function () -> 
                              sieve (lfilter (fun x -> x mod p <> 0) (nf())))
  in sieve (lfrom 2);;

ltake (1000, primes);;
