#use "lectureLazyList.ml";;
let lrepeat k lxs =
  let rec inLRepeat = function
      (_, LNil) -> LNil
    | (0, LCons(x, xs)) -> inLRepeat (k, xs())
    | (n, (LCons(x, xs) as original)) -> LCons(x, function () -> inLRepeat (n-1, original))
  in inLRepeat(k, lxs);;

ltake (20, lrepeat 3 (lfrom 10));;
