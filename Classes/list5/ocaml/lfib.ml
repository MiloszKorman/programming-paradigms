#use "lectureLazyList.ml";;

let lfib =
  let rec inLfib = function
      (pp, p) -> LCons(pp+p, fun () -> inLfib (p, pp+p))
  in LCons(0, fun () -> LCons(1, fun () -> inLfib (0, 1)));;

ltake (20, lfib);;
