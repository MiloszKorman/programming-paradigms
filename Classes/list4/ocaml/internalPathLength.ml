type 'a bt = Empty | Node of 'a * 'a bt * 'a bt;;

let innerPathLength t =
  let rec inInnerPathLength = function
      (Empty, _) -> 0
    | (Node(_, lt, rt), x) -> x + inInnerPathLength(lt, x+1) + inInnerPathLength(rt, x+1)
  in inInnerPathLength (t, 0);;

let tt = Node(1, 
              Node(2, 
                   Node(4, 
                        Node(8, Empty, Empty),
                        Node(9, Empty, Empty)
                       ),
                   Node(5, 
                        Node(10, Empty, Empty), 
                        Node(11, Empty, Empty)
                       )
                  ), 
              Node(3, 
                   Node(6, 
                        Node(12, Empty, Empty), 
                        Node(13, Empty, Empty)
                       ), 
                   Node(7, 
                        Node(14, Empty, Empty), 
                        Node(15, Empty, Empty)
                       )
                  )
             );;

innerPathLength tt;;
