type 'a bt = Empty | Node of 'a * 'a bt * 'a bt;;

let outerPathLength t =
  let rec inOuterPathLength = function
      (Empty, x) -> x
    | (Node(_, lt, rt), x) -> inOuterPathLength (lt, x+1) + inOuterPathLength (rt, x+1)
  in inOuterPathLength (t, 0);;

let tt = Node(1, 
              Node(2, 
                   Node(4, 
                        Node(8, Empty, Empty),
                        Node(9, Empty, Empty)
                       ),
                   Node(5, 
                        Node(10, Empty, Empty), 
                        Node(11, Empty, Empty)
                       )
                  ), 
              Node(3, 
                   Node(6, 
                        Node(12, Empty, Empty), 
                        Node(13, Empty, Empty)
                       ), 
                   Node(7, 
                        Node(14, Empty, Empty), 
                        Node(15, Empty, Empty)
                       )
                  )
             );;

outerPathLength tt;;
