type 'a bt = Empty | Node of 'a * 'a bt * 'a bt;;

let breadthBT root = 
  let rec innerBreadthBT = function
    | [] -> []
    | Empty::ts -> innerBreadthBT ts
    | Node(v, lt, rt)::ts -> v::innerBreadthBT (ts@[lt;rt])
  in innerBreadthBT(root::[]);;

let tt = Node(1, 
              Node(2, 
                   Node(4, 
                        Node(8, Empty, Empty),
                        Node(9, Empty, Empty)
                        ),
                   Node(5, 
                         Node(10, Empty, Empty), 
                         Node(11, Empty, Empty)
                   )
                  ), 
              Node(3, 
                   Node(6, 
                         Node(12, Empty, Empty), 
                         Node(13, Empty, Empty)
                   ), 
                   Node(7, 
                         Node(14, Empty, Empty), 
                         Node(15, Empty, Empty)
                   )
                  )
             );;

breadthBT tt;;
