sealed trait Graphs[A]

case class Graph[A](succ: A => List[A]) extends Graphs[A]

val g = Graph((i: Int) => i match {
  case 0 => List(3)
  case 1 => List(0, 2, 4)
  case 2 => List(1)
  case 3 => Nil
  case 4 => List(0, 2)
  case n => throw new Exception("Graph g: node " + n + " doesn't exist")
})

def depthSearch[A](g: Graph[A]): A => List[A] = (startNode: A) => {
  def search(visited: List[A]): List[A] => List[A] = (toVisit: List[A]) => toVisit match {
    case Nil => Nil
    case hd :: tl =>
      if (visited.contains(hd)) search(visited) (tl)
      else hd :: search(hd :: visited) (g.succ(hd) ++ tl)
  }

  search(Nil)(List(startNode))
}

depthSearch(g)(4)