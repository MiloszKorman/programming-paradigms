sealed trait BT[+A]
case object Empty extends BT[Nothing]
case class Node[A](elem: A, left: BT[A], right: BT[A]) extends BT[A]

val tt =
  Node(1,
    Node(2,
      Node(4,
        Empty,
        Empty
      ),
      Empty
    ),
    Node(3,
      Node(5,
        Empty,
        Node(6,
          Empty,
          Empty
        )
      ),
      Empty
    )
  )

def breadthBT[A](bt: BT[A]): List[A] = {
  def innerBreadthBT (ts: List[BT[A]]): List[A] = ts match {
    case Nil => Nil
    case Empty::tl => innerBreadthBT(tl)
    case Node(v, lt, rt)::tl => v::innerBreadthBT(tl ++ List(lt, rt))
  }
  innerBreadthBT(List(bt))
}

breadthBT(tt)