sealed trait BT[+A]
case object Empty extends BT[Nothing]
case class Node[A](elem: A, left: BT[A], right: BT[A]) extends BT[A]

val tt =
  Node(1,
    Node(2,
      Node(4,
        Empty,
        Empty
      ),
      Empty
    ),
    Node(3,
      Node(5,
        Empty,
        Node(6,
          Empty,
          Empty
        )
      ),
      Empty
    )
  )

def internalTreePath[A] (bt: BT[A]): Int = {
  def innerInternalTreePath (t: BT[A]): Int => Int = (depth: Int) => t match {
    case Empty => 0
    case Node(v, lt, rt) => depth + innerInternalTreePath(lt)(depth+1) + innerInternalTreePath(rt)(depth+1)
  }
  innerInternalTreePath(bt)(0)
}

internalTreePath(tt)