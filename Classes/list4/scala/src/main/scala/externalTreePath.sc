sealed trait BT[+A]
case object Empty extends BT[Nothing]
case class Node[A](elem: A, left: BT[A], right: BT[A]) extends BT[A]

val tt =
  Node(1,
    Node(2,
      Node(4,
        Empty,
        Empty
      ),
      Empty
    ),
    Node(3,
      Node(5,
        Empty,
        Node(6,
          Empty,
          Empty
        )
      ),
      Empty
    )
  )

def externalTreePath[A] (bt: BT[A]): Int = {
  def innerExternalTreePath (t: BT[A]): Int => Int = (depth: Int) => t match {
    case Empty => depth
    case Node(_, lt, rt) => innerExternalTreePath(lt)(depth+1) + innerExternalTreePath(rt)(depth+1)
  }
  innerExternalTreePath(bt)(0)
}

externalTreePath(tt)