package ping_pong

import akka.actor.{Actor, Props}

class Player(msg: String) extends Actor {
  override def receive: Receive = {
    case Start(left) =>
      println(msg)
      context.actorOf(Props(classOf[Player], "Pong")) ! Mid(left-1)
    case Mid(left) =>
      if (left <= 0) context.system.terminate
      else {
        println(msg)
        sender ! Mid(left-1)
      }
  }
}
