package ping_pong

import akka.actor.{ActorSystem, Props}

object Match {
  def main(args: Array[String]): Unit = {
    val Match = ActorSystem("Match")
    val ping = Match.actorOf(Props(classOf[Player], "Ping"))
    ping ! Start(2)
  }
}
