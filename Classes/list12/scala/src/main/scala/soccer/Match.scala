package soccer

import akka.actor.{ActorRef, ActorSystem, Props}

object Match {
  def main(args: Array[String]): Unit = {
    val Match = ActorSystem("Match")
    var players: List[ActorRef] = Nil
    val p1 = Match.actorOf(Props(new Player(0, "Player 1", players)))
    val p2 = Match.actorOf(Props(new Player(1, "Player 2", players)))
    val p3 = Match.actorOf(Props(new Player(2, "Player 3", players)))
    players = p1::p2::p3::Nil
    p1 ! Start
  }
}
