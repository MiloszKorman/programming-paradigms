package soccer

import akka.actor.{Actor, ActorRef}

import scala.util.Random

class Player(number: Int, name: String, players: =>List[ActorRef]) extends Actor {

  private val randomizer = new Random

  override def receive: Receive = {
    case Start =>
      println(name + " starts")
      randomPlayer ! Ball(1)
    case Ball(n) =>
      println(name + " " + n)
      randomPlayer ! Ball(n + 1)
  }

  private def randomPlayer(): ActorRef = {
    var rand = number
    do {
      rand = randomizer.nextInt(players.length)
    } while (rand == number)
    players(rand)
  }
}
