let rec listLength xs =
  if (xs = []) then 0
  else 1 + listLength (List.tl xs);;

print_int (listLength []);; (* 0 *)
print_string "\n";;
print_int (listLength [2;3;4;5;6]);; (* 5 *)
