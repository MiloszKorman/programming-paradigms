let rec replicate (x, n) =
  if (n <= 0) then []
  else x::replicate (x, n-1);;



let rec print_list = function 
    [] -> ()
  | e::l -> print_int e ; print_string " " ; print_list l;;

print_list (replicate (3, 3));; (* 3 3 3 *)

