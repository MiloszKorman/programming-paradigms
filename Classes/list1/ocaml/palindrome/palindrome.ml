let palindrome xs = xs = (List.rev xs);;

print_string (string_of_bool (palindrome []));; (* true *)
print_string "\n";;
print_string (string_of_bool (palindrome ('a'::'l'::'a'::[])));; (* true *)
print_string "\n";;
print_string (string_of_bool (palindrome ("a"::"b"::"c"::[])));; (* false *)