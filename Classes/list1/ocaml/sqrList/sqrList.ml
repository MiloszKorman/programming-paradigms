let rec sqrList xs =
  if (xs = []) then []
  else ((List.hd xs) * (List.hd xs))::sqrList(List.tl xs);;

print_string (String.concat " " (List.map string_of_int (sqrList [1;2;3;4])));; (* 1 4 9 16 *)
print_string "\n";;
print_string (String.concat " " (List.map string_of_int (sqrList [])));; (*  *)