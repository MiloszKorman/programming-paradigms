let rec count (x, xs) =
  if (xs = []) then 0
  else if ((List.hd xs) = x) then 1 + count(x, List.tl xs)
  else count(x, List.tl xs);;


print_int (count (1, 2::3::1::2::1::[]));; (* 2 *)
print_string "\n";;
print_int (count (1, []));; (* 0 *)