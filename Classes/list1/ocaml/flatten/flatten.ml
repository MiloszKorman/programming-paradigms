let rec flatten xss = 
  if (xss = []) then []
  else (List.hd xss)@flatten(List.tl xss);;

let rec print_list = function 
    [] -> ()
  | e::l -> print_int e ; print_string " " ; print_list l;;

print_list (flatten [[1;2];[3;4]]);; (* 1 2 3 4 *)