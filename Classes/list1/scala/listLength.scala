def listLength[A] (xs: List[A]): Int = {
    if (xs.isEmpty) 0 
    else 1 + listLength(xs.tail)
}
