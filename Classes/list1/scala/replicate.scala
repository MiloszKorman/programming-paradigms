def replicate[A] (x: A, n: Int): List[A] = {
  if (n == 0) Nil
  else x::replicate(x, n-1)
}
