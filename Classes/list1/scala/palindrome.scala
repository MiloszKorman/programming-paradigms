//compare strings
def palindrome[A] (xs: List[A]): Boolean = xs == xs.reverse
