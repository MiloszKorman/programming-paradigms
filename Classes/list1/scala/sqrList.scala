//using map
def sqrList (xs: List[Int]): List[Int] = xs.map(x => x*x)

//and not - recursively
def sqrList (xs: List[Int]): List[Int] = {
    if (xs.isEmpty) Nil
    else (xs.head * xs.head)::sqrList(xs.tail)
}
