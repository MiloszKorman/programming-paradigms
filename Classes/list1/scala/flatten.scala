def flatten[A] (xss: List[List[A]]): List[A] = {
    if (xss.isEmpty) Nil
    else xss.head:::flatten(xss.tail)
}
