package exceptions

object ExceptionsUse {
  def main(args: Array[String]): Unit = {
    try {
      this.method1
    } catch {
      case ex: Exception => ex.printStackTrace()
    }
  }

  def method1: Unit = {
    method2
  }

  def method2: Unit = {
    method3
  }

  @throws
  def method3: Unit = {
    throw new Exception("Exception in method 3")
  }
}
