package time1

object Time1Test {
  def main(args: Array[String]): Unit = {
    var time1 = new Time1(13)
    println(time1.hour)
    time1.hour = 15
    println(time1.hour)
    time1.hour = -14
    println(time1.hour)
    var time2 = Time1(13)
    println(time2.hour)
  }
}
