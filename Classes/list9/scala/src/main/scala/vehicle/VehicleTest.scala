package vehicle

object VehicleTest {
  def main(args: Array[String]): Unit = {
    val myCar = new Vehicle("Mercedes", "C", "22224")
    println(myCar.producer)
    println(myCar.model)
    println(myCar.yOfProd)
    println(myCar.registrationNum)
  }
}
