package vehicle

class Vehicle(val producer: String, val model: String, val yOfProd: Int = -1, var registrationNum: String = "") {
  def this(producer: String, model: String, registrationNumber: String) {
    this(producer, model, -1, registrationNumber)
  }
}
