package time3

class Time3(private var h: Int, private var m: Int) {
  require(0 <= h && h < 24)
  require(0 <= m && m < 60)

  private var minutesGlobal: Int = h * 60 + m

  def hour: Int = minutesGlobal / 60

  def minute: Int = minutesGlobal % 60

  def hour_=(newHour: Int): Unit = {
    require(0 <= newHour && newHour < 24)
    minutesGlobal = minute + newHour * 60
  }

  def minute_=(newMinute: Int): Unit = {
    require(0 <= newMinute && newMinute < 60)
    minutesGlobal = hour * 60 + newMinute
  }

  def before(other: Time3): Boolean = {
    minutesGlobal < other.minutesGlobal
  }

}
