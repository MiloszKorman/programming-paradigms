package time2

object Time2Test {
  def main(args: Array[String]): Unit = {
    var time1 = new Time2(12, 50)
    println(time1.hour)
    println(time1.minute)
    time1.minute = 13
    println(time1.hour)
    println(time1.minute)

    var time2 = new Time2(10, 2)
    println(time2.before(time1))
  }
}
