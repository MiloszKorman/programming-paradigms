package time2

class Time2(private var h: Int, private var m: Int) {
  require(0 <= h && h < 24)
  require(0 <= m && m < 60)

  def hour: Int = h

  def minute: Int = m

  def hour_=(newHour: Int): Unit = {
    require(0 <= newHour && newHour < 24)
    h = newHour
  }

  def minute_=(newMinute: Int): Unit = {
    require(0 <= newMinute && newMinute < 60)
    m = newMinute
  }

  def before(other: Time2): Boolean = {
    if (h == other.h) m < other.m
    else h < other.h
  }

}
